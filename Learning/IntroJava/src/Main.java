
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {

        int a = 7; // it reads an integer value from the standard input
        int b = 9; // it reads another integer value from the standard input
        System.out.println(b +" "+ a); // it writes the result of a + b in the standard output
    }
}