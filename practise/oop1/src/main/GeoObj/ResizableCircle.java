public class ResizableCircle extends ACircle implements Resizable {

    public ResizableCircle(double radius){
        super(radius);
    }

    @Override
    public double resize(int percent) {
        super.radius = this.radius* ((percent/100)*1);
        return radius;
    }
}
