public class ACircle implements GeometricObject {

    double radius = 1.0;

    public ACircle(double radius){
        this.radius = radius;
    }

    @Override
    public double getPerimeter() {

        return radius * radius * Math.PI;
    }

    @Override
    public double getArea() {
        return 2 * Math.PI * radius;
    }

    @Override
    public String toString() {
        return "I am a Circle radius " + radius + "  Perimeter  " + getPerimeter() + "  Area  "  + getArea();
    }
}
