public class MovableCircle implements Movable {

    private int radius;
    private MovablePoint center;

    public MovableCircle(int x, int y, int xSpeed, int ySpeed, int radius){
        center = new MovablePoint(x, y, xSpeed, ySpeed){

        };
        this.radius = radius;
    }

    @Override
    public void moveUp() {
        center.y -= center.ySpeed;
    }

    @Override
    public void moveRight() {
        center.x += center.xSpeed;
    }

    @Override
    public void moveLeft() {
        center.x -= center.xSpeed;
    }

    @Override
    public void moveDown() {
        center.y += center.ySpeed;
    }

    @Override
    public String toString() {
        return " I am a Movable Circle, with the following details [X "
                + center.x
                + " Y "
                + center.y
                + " X-Speed "
                + center.xSpeed
                + " Y-Speed "
                + center.ySpeed
                + " the radius is "
                + radius;
    }
}
