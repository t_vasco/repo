public class Circle {
    private double radius;
    private String color;

    public Circle(){
        radius = 1.0;
        color = "Red";
    }

    public Circle(double r){
        this.radius = radius;
        this.color = "Red";
    }

    public Circle(double r, String c){
        radius = r;
        color = c;
    }

    public double getRadius() {
        return radius;
    }

    public String getColor() {
        return color;
    }

    public double getRadios(){
        return radius;
    }

    public double getArea(){
        return radius*radius*Math.PI;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public String toString(){
        return "Circle[radius = " + radius + " color = " + color + "]";
    }
}
