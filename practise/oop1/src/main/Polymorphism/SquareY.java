public class SquareY extends RectangleY {
    public SquareY(){

    }

    public SquareY(double side){
        this.length = side;
        this.width = side;
    }

    public SquareY(double side, String color, boolean filled){
        this.width = side;
        this.length = side;
        this.color = color;
        this.filled = filled;
    }

    @Override
    public double getLength() {
        return super.getLength();
    }
    public void setSide(double x){
        this.length = x;
        this.width = x;
    }

    @Override
    public void setWidth(double width) {
        setSide(width);
    }

    @Override
    public void setLength(double length) {
        setWidth(length);
    }

    @Override
    public String toString() {
        return "This is a square, the side is [" + length + "] the area is [" + getArea() + "] the perimeter is [" + getPerimeter() + "]";
    }

}
