public class CircleY extends Shape {

    protected double radius;

    public CircleY(){

    }
    public CircleY(double radius){
        this.radius = radius;
    }

    public CircleY(double radius, String color, boolean filled){
        this.radius = radius;
        this.color = color;
        this.filled = filled;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {
        double area = radius * radius * Math.PI;
        return area;
    }

    @Override
    public double getPerimeter() {
        double perimeter = 2 * Math.PI * radius;
        return perimeter;
    }

    @Override
    public String toString() {
        return "This is a circle, the radius is [" + radius + "] the area is [" + getArea() + "] the perimeter is [" + getPerimeter() + "]";
    }


}
