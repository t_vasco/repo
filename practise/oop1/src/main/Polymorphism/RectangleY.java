public class RectangleY extends Shape{

    protected double width;

    protected double length;

    public RectangleY(){

    }

    public RectangleY(double width, double length){
        this.width = width;
        this.length = length;
    }

    public RectangleY(double width, double length, String color, boolean filled){
        this.width = width;
        this.length = length;
        this.color = color;
        this.filled = filled;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public double getArea() {
        double area = width*length;
        return area;
    }

    @Override
    public double getPerimeter() {
        double perimeter = 2 * (width * length);
        return perimeter;
    }

    @Override
    public String toString() {
        return "This is a rectangle, the width is [" + width + "] the length is [" + length +"] the area is [" + getArea() + "] the perimeter is [" + getPerimeter() + "]";
    }

}
