public class Line {

    private Point begin;
    private Point end;

    public Line (Point begin, Point end){
        this.begin = begin;
    }

    public Line(int beginX, int beginY, int endX, int endY){
        begin = new Point(beginX, beginY);
        end = new Point(endX, endY);
    }

    public String toString(){
         return "The Beginning is [" + begin + "]. The end is [" + end + "].";
    }

    public Point getBegin() {
        return begin;
    }

    public Point getEnd() {
        return end;
    }

    public void setBegin(Point begin) {
        this.begin = begin;
    }

    public void setEnd(Point end) {
        this.end = end;
    }


    public int getBeginX(){
        return begin.getX();
    }
    public int getBeginY(){
        return begin.getY();
    }
    public int getEndX(){
        return end.getX();
    }
    public int getEndY(){
        return end.getY();
    }


    public void setBeginX(int beginX){
        this.begin.setX(beginX);
    }
    public void setBeginY(int beginY){
        this.begin.setY(beginY);
    }
    public void setBeginXY(int beginX, int beginY){
        this.setBeginX(beginX);
        this.setBeginY(beginY);
    }
    public void setEndX(int endX){
        this.setEndX(endX);
    }
    public void setEndY(int endY){
        this.setEndY(endY);
    }
    public void setEndXY(int endX, int endY){
        this.setEndX(endX);
        this.setEndY(endY);
    }

    public int getLength(){
        return (int) Math.sqrt(getBeginX()*getEndX() + getBeginY() * getBeginY());
    }

    public double getGradient(){
        return Math.atan2(getBeginX(), getBeginY());
    }

}
