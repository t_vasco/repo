public class LineSub extends Point {

    private Point end;



    public LineSub(int beginX, int beginY, int endX, int endY){
        super(beginX, beginY);
        this.end = new Point(endX, endY);
    }

    public LineSub(Point begin, Point end){
        super(begin.getX(), begin.getY());
        this.end = end;
    }

    public String toString(){
        return "The begin is [" + super.getX() + "] and the end is [" + super.getY() + "].";
    }


    public int getBeginX(){
        return super.getX();
    }

    public int getBeginY(){
        return super.getY();
    }

    public int getEndX(){
        return super.getX();
    }

    public int getEndY(){
        return super.getY();
    }

    public void setBeginX(int x){
        super.setX(x);
    }
    public void setBeginY(int y){
        super.setY(y);
    }
    public void setBeginXY(int x, int y){
        super.setX(x);
        super.setY(y);
    }
    public void setEndX(int x){
        super.setX(x);
    }
    public void setEndY(int y){
        super.setY(y);
    }
    public void setEndXY(int x, int y){
        super.setX(x);
        super.setY(y);
    }

    public int getLegth(){
        return (int) Math.sqrt(getBeginX()*getEndX() + getBeginY() * getBeginY());

    }

    public double getGradient(){
        return Math.atan2(getBeginX(), getBeginY());

    }

}
