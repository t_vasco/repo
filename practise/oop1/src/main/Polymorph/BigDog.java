public class BigDog extends Dog {
    @Override
    public void greeting() {
        System.out.println("WOOW!");
    }

    @Override
    public void greeting(Dog another) {
        System.out.println("WWWOOOWWWW!");
    }
}
