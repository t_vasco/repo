public class MyPoint {

    private int x = 0;
    private  int y = 0;

    public MyPoint(){}

    public MyPoint(int x, int y){
        this.x = x;
        this.y = y;
    }



    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int[] getXY(){
        int[] xyArr = new int[2];
        xyArr[0] = x;
        xyArr[1] = y;
        return xyArr;
    }

    public void setXY(int x, int y){
        this.x =x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "x is " + x + "Y is " + y;
    }

    public double distance (int x, int y){
        double distanceX = this.x - x;
        double distanceY = this.y -y;

        return Math.sqrt(distanceX*distanceX + distanceY*distanceY);

    }

    public double distance(MyPoint aPoint){

        double distanxeX = this.x - aPoint.x;
        double distanceY = this.y -aPoint.y;
        return Math.sqrt(distanceY*distanceY + distanxeX * distanxeX);
    }

    public double distance(){
        double distanceX = this.x - 0;
        double distanceY = this.y -0;
        return Math.sqrt(distanceX*distanceX + distanceY*distanceY);
    }




}
