public class TestAnimal {

    public static void main(String[] args) {
        // Using the subclasses
        Cat cat1 = new Cat();
        System.out.println("here1");
        cat1.greeting();
        Dog dog1 = new Dog();
        System.out.println("here 2");
        dog1.greeting();
        BigDog bigDog1 = new BigDog();
        System.out.println("here 3");
        bigDog1.greeting();

        // Using Polymorphism
        Animal animal1 = new Cat();
        System.out.println("here 4");
        animal1.greeting();
        Animal animal2 = new Dog();
        System.out.println("here 5" );
        animal2.greeting();
        Animal animal3 = new BigDog();
        System.out.println("here 6");
        animal3.greeting();
        //ERROR Animal animal4 = new Animal(); Can't make a new instance of animal.

        // Downcast
        Dog dog2 = (Dog)animal2;
        BigDog bigDog2 = (BigDog)animal3;
        Dog dog3 = (Dog)animal3;
        System.out.println("here7");
        //ERROR Cat cat2 = (Cat)animal2; This animal was a Dog it does not have a reference for a Cat
        dog2.greeting(dog3);
        System.out.println("here 8");
        dog3.greeting(dog2);
        System.out.println("here 9");
        dog2.greeting(bigDog2);
        System.out.println("here 10");
        bigDog2.greeting(dog2);
        System.out.println("here 11");
        bigDog2.greeting(bigDog1);
    }
}
