public class TestMyPoint {

    public static void main(String[] args) {


        MyPoint p1 = new MyPoint();

        System.out.println(p1);

        p1.setX(8);
        p1.setY(9);

        System.out.println(p1);

        p1.setXY(20,20);

        System.out.println(p1);

        System.out.println(p1.getXY());


        MyPoint p2 = new MyPoint(15, 20);

        System.out.println(p1.distance(p2));

        System.out.println(p2.distance(p1));

        System.out.println(p1.distance(5,6));

        System.out.println(p1.distance());


        MyPoint[] points = new MyPoint[10];

        for(int i = 0; i<points.length; i++){
            points[i] = new MyPoint(i,i);
        }

       for(int j = 0; j<points.length; j++){
           System.out.println(points[j].getX() + points[j].getY());
       }
    }
}
