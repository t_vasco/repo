public class TestMovable {

    public static void main(String[] args) {


        Movable m1 = new MovablePoint(5, 6, 10, 15);
        System.out.println(m1);
        System.out.println("");
        m1.moveLeft();
        System.out.println(m1);

        Movable m2 = new MovableCircle(1,2,3,4,20);
        System.out.println(" ");
        System.out.println(m2);
        m2.moveRight();
        System.out.println(" ");
        System.out.println(m2);
        System.out.println(" ");



        Movable m3 = new MovableRectangle(5,5,15,10,5,5);

        System.out.println(m3);

        m3.moveRight();
        System.out.println(" ");
        System.out.println("Move Right" + m3);

        m3.moveDown();
        System.out.println(" ");
        System.out.println("Move Down " + m3);

        m3.moveLeft();
        System.out.println(" ");
        System.out.println("Move Left " + m3);

    }
}
