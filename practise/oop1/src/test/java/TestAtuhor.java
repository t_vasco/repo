public class TestAtuhor {

    public static void main(String[] args) {

        Author ahTeck = new Author("Tan Ah Teck", "ahteck@nowhere.com", 'm');

        Author tiagoVasco = new Author("Tiago Vasco", "tiago.jvasco@gmail.com", 'm');
        Author[] authors = new Author[2];

        authors[0] = tiagoVasco;
        authors[1] = ahTeck;
        System.out.println(ahTeck);

        System.out.println("name is " + ahTeck.getName());

        System.out.println("email is " + ahTeck.getEmail());

        Book aNewBook = new Book("This Book", authors, 50.00, 15 );

        Book anotherBook = new Book("Another Book", authors, 20.00);

        anotherBook.setPrice(30.00);

        System.out.println(aNewBook.getName());

        System.out.println(aNewBook.getQty());

        System.out.println(anotherBook.getPrice());

        System.out.println(aNewBook.getAuthor());

        System.out.println(anotherBook.getAuthor());


        System.out.println(anotherBook);

        System.out.println(aNewBook);

        //System.out.println(aNewBook.getAuthor[].getEmail() +" " + anotherBook.getAuthor().getEmail());



    }
}
