public class Customer {
    private String name;
    private boolean member = false;
    private String memberType;

    public enum Type{
        PLATINUM,
        GOLD,
        SILVER
    }

    public Customer(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isMember() {
        return member;
    }

    public void setMember(boolean member) {
        this.member = member;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(Type memberType) {
        this.memberType = memberType.toString();
    }

    @Override
    public String toString() {
        return "Hello, i am " + name + " membership [" + member + "] level [ " + memberType + "]";
    }

}
