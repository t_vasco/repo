public class DiscountRate {

    private static final double SERVICE_DISCOUNT_PREMIUM= 0.2;
    private static final double SERVICE_DISCOUNT_GOLD = 0.15;
    private static final double SERVICE_DISCOUNT_SILVER = 0.10;

    private static final double PRODUCT_DISCOUNT_PREMIUM = 0.1;
    private static final double PRODUCT_DISCOUNT_GOLD = 0.1;
    private static final double PRODUCT_DISCOUNT_SILVER = 0.1;

    public static double getServiceDiscountRate(String discountType){
       if(discountType == "Premium"){
           return SERVICE_DISCOUNT_PREMIUM;
       }
       if(discountType == "Gold"){
           return SERVICE_DISCOUNT_GOLD;
       }
       if(discountType == "Silver"){
           return SERVICE_DISCOUNT_SILVER;
       }
       return 0.0;
    }

    public static double getProductDiscount(String discountType) {
        if(discountType == "Premium"){
            return PRODUCT_DISCOUNT_PREMIUM;

        }
        if(discountType == "Gold"){
            return PRODUCT_DISCOUNT_GOLD;
        }
        if(discountType == "Silver"){
            return PRODUCT_DISCOUNT_SILVER;
        }
        return 0.0;
    }


}
