import java.util.Date;

public class Visit {

    private Customer customer;

    private int date;

    private double serviceExpense;

    private double productExpense;

    public Visit(String name, int date){
       this.customer = new Customer(name);
        this.date = date;
    }

    public String getName(){
      return customer.getName();
    }

    public double getServiceExpense() {
        return serviceExpense;
    }

    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }

    public double getProductExpense() {
        return productExpense;
    }

    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }

    public double getTotalExpense(){
        double total = getProductExpense() + getServiceExpense();
        return total;
    }

    @Override
    public String toString() {
        return  customer
                + " my Product Expense is €" + getProductExpense()
                + " my Service Expense is €" + getServiceExpense()
                + " my total is €" + getTotalExpense();
    }
}
