package org.academiadecodigo.codeforall.hotel;

public class Person {

    //Person properties
    private String person1Name;
    private Hotel hotel;
    private int roomNum;


    //Person Methods

    public Person(){}

    public void doCheckIn(){
        hotel.pCheckIn();
    }

    public void doCheckout(int roomNum){
        hotel.pCheckOut(roomNum);

    }

    //Getting person name
    public String getPerson1Name(){
        return person1Name;
    }

    //Getting roomNum


    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }



}
