package org.academiadecodigo.codeforall.hotel;

public class Room {

    //Properties from the room
    private boolean roomTaken = false;
    private int roomNum;


    public Room (int roomNum, boolean roomTaken){
        this.roomNum = roomNum;
        this.roomTaken = roomTaken;
    }



    //Getters and Setters

    //Get and Set if the room is taken or not
    public boolean getRoomTaken(){
        return roomTaken;
    }
    public void setRoomTaken(boolean roomTaken){
        this.roomTaken = roomTaken;
    }

    //Getting the room number
    public int getRoomNum(){
        return roomNum;
    }
    public void setRoomNum(int roomNum){
        this.roomNum = roomNum;
    }

}
