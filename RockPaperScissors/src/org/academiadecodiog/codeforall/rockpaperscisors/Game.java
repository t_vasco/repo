package org.academiadecodiog.codeforall.rockpaperscisors;

public class Game {

    private Player player1;
    private Player player2;
    private int round = 0;
    private int player1Victories = 0;
    private int player2Victories = 0;


    public void startGame(){

        Player player1 = new Player("Julieta");
        Player player2 = new Player("Vanessa");

        while(round < 10 ) {


            Hand player1Hand = player1.getOption();
            Hand player2Hand = player2.getOption();


            System.out.println(player1.getName() + " plays " + player1Hand);
            System.out.println(player2.getName() + " plays " + player2Hand);

            round++;
            System.out.println("The current round is " + round);

            if (player1Hand == player2Hand) {
                System.out.println("It's a tie suckers, no winners this time!!");
            } else if ((player1Hand == Hand.PAPER && player2Hand == Hand.ROCK)
                    || (player1Hand == Hand.ROCK && player2Hand == Hand.SCISORS)
                    || (player1Hand == Hand.SCISORS && player2Hand == Hand.PAPER)) {
                System.out.println(player1.getName() + "is the winner");
                player1Victories ++;


            } else {
                System.out.println(player2.getName() + " is the winner");
                player2Victories ++;
            }
            System.out.println(player1Victories);
            System.out.println(player2Victories);

        }
        if(player1Victories > player2Victories){
            System.out.println(player1.getName() +" is the final winner.");
        } else{
            System.out.println(player2.getName() + " is the final winner.");
        }
    }
}
