package org.academiadecodiog.codeforall.rockpaperscisors;

public class Main {


    public static void main(String[] args){
        Player player1 = new Player("Juelieta");
        Player player2 = new Player("Vanessa");

        Game startNewGame = new Game();
        startNewGame.startGame();

    }
}
