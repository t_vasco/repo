package org.academiadecodiog.codeforall.rockpaperscisors;

public class Player {

    private String name;
    public int victories = 0;

    public Player(String name){
        this.name = name;
    }

    public Hand getOption() {
        int randomIndex = (int)(Math.random()* Hand.values().length);

        return Hand.values()[randomIndex];
    }


    public String getName(){
        return name;
    }

    public int getVictories(){
        return victories;
    }
    public void setCount(int victories){
        this.victories=victories;
    }
    public void increment(int increment){
        setCount(getVictories()+1);
    }



}

