package org.academiadecodiog.codeforall.rockpaperscisors;

public enum Hand {

    ROCK ("ROCK"),
    PAPER ("PAPER"),
    SCISORS("SCISSORS");

    private String myOption;

    Hand(String myOption){
        this.myOption = myOption;
    }

    public String getmyOption(){
        return this.myOption;

    }
}
