package org.academiadecodigo.codeforall;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.codeforall.connector.ConnectionManager;
import org.academiadecodigo.codeforall.controller.LoginController;
import org.academiadecodigo.codeforall.controller.MainController;
import org.academiadecodigo.codeforall.controller.UserDetailsController;
import org.academiadecodigo.codeforall.controller.UserListController;
import org.academiadecodigo.codeforall.model.User;
import org.academiadecodigo.codeforall.service.JdbcUserService;
import org.academiadecodigo.codeforall.service.MockUserService;
import org.academiadecodigo.codeforall.service.UserService;
import org.academiadecodigo.codeforall.utils.Security;
import org.academiadecodigo.codeforall.view.LoginView;
import org.academiadecodigo.codeforall.view.MainView;
import org.academiadecodigo.codeforall.view.UserDetailsView;
import org.academiadecodigo.codeforall.view.UserListView;

public class App {

    public static void main(String[] args) {

        LoginController loginController = new LoginController();
        MainController mainController = new MainController();
        UserListController userListController = new UserListController();
        LoginView loginView = new LoginView();
        MainView mainView = new MainView();
        UserListView userListView = new UserListView();
        UserDetailsController userDetailsController = new UserDetailsController();
        UserDetailsView userDetailsView = new UserDetailsView();
        Prompt prompt = new Prompt(System.in, System.out);

        ConnectionManager connectionManager = new ConnectionManager();
        JdbcUserService jdbcUserService = new JdbcUserService(connectionManager);



        //UserService userService = new MockUserService();
        jdbcUserService.add(new User("rui", "ferrao@academiadecodigo.org", Security.getHash("academiadecodigo"),
                "Rui", "Ferrão", "912345678"));
        jdbcUserService.add(new User("faustino", "faustino@academiadecodigo.org", Security.getHash("academiadecodigo"),
                "João", "Faustino", "966666666"));
        jdbcUserService.add(new User("audrey", "audrey@academiadecodigo.org", Security.getHash("academiadecodigo"),
                "Audrey", "Lopes", "934567890"));


        // Wire login controller and view
        loginView.setPrompt(prompt);
        loginView.setController(loginController);
        loginController.setUserService(jdbcUserService);
        loginController.setView(loginView);
        loginController.setNextController(mainController);

        // Wire main controller and view
        mainView.setPrompt(prompt);
        mainView.setController(mainController);
        mainController.setView(mainView);
        mainController.setUserListController(userListController);
        mainController.setUserDetailsController(userDetailsController);

        // Wire userList controller and view
        userListView.setPrompt(prompt);
        userListView.setController(userListController);
        userListController.setUserService(jdbcUserService);
        userListController.setView(userListView);

        // Wire userDetails controller and view
        userDetailsView.setPrompt(prompt);
        userDetailsView.setController(userDetailsController);
        userDetailsController.setUserService(jdbcUserService);
        userDetailsController.setView(userDetailsView);


        // Start APP
        loginController.init();
        connectionManager.close();

    }
}
