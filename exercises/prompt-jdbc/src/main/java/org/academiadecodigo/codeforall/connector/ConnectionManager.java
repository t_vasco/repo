package org.academiadecodigo.codeforall.connector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {

    private Connection connectionEstablished = null;

    public Connection getConnection(){
        try{
            if(connectionEstablished == null){
                connectionEstablished = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/ac?serverTimezone=UTC", "root", "");

            }
        }catch (SQLException ex){
            System.out.println("SQL Connection Exception when making a connection: ");
            ex.printStackTrace();
        }

        return connectionEstablished;
    }

    public void close(){
        try{
            if(connectionEstablished != null){
                connectionEstablished.close();

            }
        } catch (SQLException ex){
            System.out.println(" Failled to close connection: ");
            ex.printStackTrace();
        }
    }

}
