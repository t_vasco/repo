package org.academiadecodigo.codeforall.service;


import org.academiadecodigo.codeforall.connector.ConnectionManager;
import org.academiadecodigo.codeforall.model.User;
import org.academiadecodigo.codeforall.utils.Security;

import javax.swing.plaf.nimbus.State;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class JdbcUserService implements UserService{

    private Connection dbConnection;
    private ConnectionManager connectionManager;

    public JdbcUserService(ConnectionManager connection){
        this.connectionManager = connection;
        this.dbConnection = connectionManager.getConnection();
    }

    @Override
    public boolean authenticate(String username, String password){

        startConnection();
        String user = null;
        String pass = null;
        try {

            Statement statement = dbConnection.createStatement();

            String query = "SELECT username, password FROM user";

            ResultSet resultSet = statement.executeQuery(query);

            if(resultSet.next()){
                user = resultSet.getString("username");

                pass = resultSet.getString("password");

            }


        }catch (SQLException ex){
            System.out.println("Error Authenticating user: ");
            ex.printStackTrace();
        }

        return (user.equals(username) && pass.equals(Security.getHash(password)) );
    }


    @Override
    public void add(User user) {

        startConnection();


       if(findByName(user.getUsername()) != null){
           return;
       }
        try {
            Statement statement = dbConnection.createStatement();



            String query = "INSERT INTO user (username, password, email, firstname, lastname, phone) " +
                    "VALUES ('" + user.getUsername() + "','"
                    +  user.getPassword() +"','"
                    + user.getEmail() +"','"
                    + user.getFirstName() +"','"
                    + user.getLastName() +"','"
                    + user.getPhone() + "')";

            statement.executeUpdate(query);

            statement.close();

        }catch (SQLException ex){
            System.out.println("Exception found adding user: ");
            ex.printStackTrace();
        }
    }


    @Override
    public User findByName(String username) {
        try {
            Statement statement = dbConnection.createStatement();
            String query = "Select * FROM user WHERE username = '" + username + "';"  ;

            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()){
                User foundUser = new User(
                        resultSet.getString("username"),
                        resultSet.getString("firstname"),
                        resultSet.getString("lastname"),
                        resultSet.getString("phone"),
                        resultSet.getString("email"));
                    return foundUser;
            }
            statement.close();

        }catch (SQLException ex){
            System.out.println("Exception finding name: " + username);
            ex.printStackTrace();
        }

        return null;
    }


    @Override
    public List<User> findAll() {
        List<User> list = new LinkedList<>();
        try{
            Statement statement = dbConnection.createStatement();
            String query = "Select * From user;";
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()){

                User foundUser = new User(
                        resultSet.getString("username"),
                        resultSet.getString("email"));                        ;
                list.add(foundUser);
            }

            statement.close();
        }catch (SQLException ex){
            System.out.println("Exception listing users: ");
            ex.printStackTrace();
        }


        return list;
    }

    @Override
    public int count() {

        int result = 0;

        try {
            Statement statement = dbConnection.createStatement();

            String query = "SELECT COUNT('username') FROM user";

            ResultSet resultSet = statement.executeQuery(query);

            if(resultSet.next()){
                result = resultSet.getInt("username");
                statement.close();
            }
            return result;
        }catch (SQLException ex){
            System.out.println("Exception counting users: ");
            ex.printStackTrace();
        }


        return 0;
    }

    private void startConnection() {
        try{
            if(dbConnection.isClosed()){
                dbConnection = connectionManager.getConnection();
            }
        }catch (SQLException ex){
            System.out.println("Could not establish Connection: ");
            ex.printStackTrace();

        }    }
}
