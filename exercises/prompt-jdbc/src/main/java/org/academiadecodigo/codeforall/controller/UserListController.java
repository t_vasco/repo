package org.academiadecodigo.codeforall.controller;

import org.academiadecodigo.codeforall.model.User;

import java.util.List;

public class UserListController extends AbstractController {

    public List<User> getUserList() {
        return userService.findAll();
    }

}
