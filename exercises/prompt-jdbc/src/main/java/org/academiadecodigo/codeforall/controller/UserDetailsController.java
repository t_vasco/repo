package org.academiadecodigo.codeforall.controller;

import org.academiadecodigo.codeforall.model.User;

public class UserDetailsController extends AbstractController {


    public User getUser(String username) {
        return userService.findByName(username);
    }
}
