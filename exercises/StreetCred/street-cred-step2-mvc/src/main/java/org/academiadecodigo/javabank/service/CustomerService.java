package org.academiadecodigo.javabank.service;

import org.academiadecodigo.javabank.model.Customer;

import java.util.Set;

public interface CustomerService {


    Customer getId(Integer id);

    double getBalance(int customerId);


    void add(Customer customer);

    Set<Integer> getCustomersIds();

    /*Customer get(Integer id);
    List<Customer> list();
    Set<Integer> listCustomerAccountIds(Integer id);
    */
}


