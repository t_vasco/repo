package org.academiadecodigo.javabank.controller;

import org.academiadecodigo.javabank.managers.AccountManager;
import org.academiadecodigo.javabank.model.account.Account;
import org.academiadecodigo.javabank.model.account.AccountType;
import org.academiadecodigo.javabank.service.CAuthService;
import org.academiadecodigo.javabank.view.NewAccountView;

/**
 * The {@link NewAccountView} controller
 */
public class NewAccountController extends AbstractController {

    private Integer newAccountId;

    /**
     * Gets the new account id
     *
     * @return the new account id
     */
    public Integer getNewAccountId() {
        return newAccountId;
    }

    /**
     * Creates a new {@link Account}
     *
     * @see Controller#init()
     * @see AccountManager#openAccount(AccountType)
     */
    @Override
    public void init() {

        newAccountId = createAccount();
        super.init();
    }



    private int createAccount() {

        Account newAccount = cAccountService.openAccount(AccountType.CHECKING);
        getLoginCustomer().addAccount(newAccount);

        return newAccount.getId();
    }


}
