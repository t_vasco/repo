package org.academiadecodigo.javabank.service;


import org.academiadecodigo.javabank.model.Customer;

import java.util.HashMap;
import java.util.Set;

public class CAuthService implements AuthService {

    private HashMap<Integer, Customer> customers;

    private int loginCustomer;

    public CAuthService(){
        this.customers = new HashMap<>();
    }


    @Override
    public boolean authenticate(Integer id) {
        return true;
    }

    @Override
    public Customer getAccessingCustomer() {

        return customers.get(loginCustomer);
    }

    public void setLoginCustomer(int id) {
        this.loginCustomer = id;
    }

    public Set<Integer> getCustomersIds() {
        return customers.keySet();
    }

    public void addCustomer(Customer customer){
        customers.put(customer.getId(), customer);
    }


}
