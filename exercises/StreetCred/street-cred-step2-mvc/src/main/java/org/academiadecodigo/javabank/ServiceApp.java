package org.academiadecodigo.javabank;

import org.academiadecodigo.javabank.controller.LoginController;
import org.academiadecodigo.javabank.service.CAuthService;
import org.academiadecodigo.javabank.service.CCustomerService;


public class ServiceApp {

    public static void main(String[] args) {

        ServiceApp serviceApp = new ServiceApp();
        serviceApp.serviceBootstrap();
    }

    private void serviceBootstrap() {

        ServiceBootstrap serviceBootstrap = new ServiceBootstrap();

        CCustomerService cCustomerService = serviceBootstrap.generateTestDataC();

        //CAuthService cAuthService = serviceBootstrap.generateTestDataC();

        LoginController loginController = serviceBootstrap.wireObjects(cCustomerService);

        // start application
        loginController.init();
    }

}
