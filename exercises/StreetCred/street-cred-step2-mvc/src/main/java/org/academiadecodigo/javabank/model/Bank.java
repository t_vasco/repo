package org.academiadecodigo.javabank.model;

import org.academiadecodigo.javabank.managers.AccountManager;
import org.academiadecodigo.javabank.model.account.Account;
import org.academiadecodigo.javabank.model.account.AccountType;

import java.util.HashMap;
import java.util.Set;

/**
 * The bank entity
 */
public class Bank {

    private AccountManager accountManager;
    private HashMap<Integer, Customer> customers;

    //private int loginCustomer;

    /**
     * Creates a new instance of Bank
     */
    public Bank() {
        this.customers = new HashMap<>();
    }

    /**
     * Gets the total balance of the bank
     *
     * @return the bank total balance
     */
    /*public double getBalance() {

        System.out.println("HEre Teste");
        double balance = 0;

        for (Customer customer : customers.values()) {
            balance += customer.getBalance();
        }

        return balance;
    }*/

    /**
     * Sets the account manager
     *
     * @param accountManager the account manager to set
     */
    public void setAccountManager(AccountManager accountManager) {
        this.accountManager = accountManager;
    }


    /**
     * Gets the ids of the bank customers
     *
     * @return customer ids
     */
   /* public Set<Integer> getCustomerIds() {
        return customers.keySet();
    }
*/
    /**
     * Gets the logged in customer
     *
     * @return the customer
     */
    /*public Customer getLoginCustomer() {
        return customers.get(loginCustomer);
    }*/

    //TODO REPLACED
   /* *//**
     * Sets the logged in customer
     *
     * @param id the customer id
     *//*
    public void setLoginCustomer(int id) {
        this.loginCustomer = id;
    }
*/
    /**
     * Adds a new customer to the bank
     *
     * @param customer the new bank customer
     */


   /* public void addCustomer(Customer customer) {
        customers.put(customer.getId(), customer);
    }
*/
    /*public Account openAccount(AccountType accountType) {
        return accountManager.openAccount(accountType);
    }*/

/*
    public void withdraw(int accountId, double amount) {
        accountManager.withdraw(accountId, amount);
    }
*/

   /* public void deposit(int accountId, double amount) {
        accountManager.deposit(accountId, amount);
    }*/
}
