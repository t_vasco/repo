package org.academiadecodigo.javabank.controller;

import org.academiadecodigo.javabank.model.Bank;
import org.academiadecodigo.javabank.model.Customer;
import org.academiadecodigo.javabank.service.CAccountService;
import org.academiadecodigo.javabank.service.CAuthService;
import org.academiadecodigo.javabank.service.CCustomerService;
import org.academiadecodigo.javabank.view.View;

/**
 * A generic controller to be used as a base for concrete controller implementations
 *
 * @see Controller
 */
public abstract class AbstractController implements Controller {

    protected View view;
    protected Bank bank;
    protected CAuthService cAuthService;
    protected CAccountService cAccountService;
    protected CCustomerService cCustomerService;

    /**
     * Sets the controller view
     *
     * @param view the view to be set
     */
    public void setView(View view) {
        this.view = view;
    }

    /**
     * Sets the controller bank
     *
     * @param bank
     */
    public void setBank(Bank bank) {
        this.bank = bank;
    }

    /**
     * @see Controller#init()
     */
    @Override
    public void init() {
        view.show();
    }



    public void setcAccountService(CAccountService cAccountService) {
        this.cAccountService = cAccountService;
    }

    public void setcAuthService(CAuthService cAuthService) {
        this.cAuthService = cAuthService;
    }

    public void setcCustomerService(CCustomerService cCustomerService) {
        this.cCustomerService = cCustomerService;
    }


    /**
     * Returns accessing customer from bank
     *
     * @return accessing customer
     */
    public Customer getLoginCustomer() {
        return cAuthService.getAccessingCustomer();
    }


}
