package org.academiadecodigo.javabank.service;

import org.academiadecodigo.javabank.model.Customer;

import java.util.HashMap;
import java.util.Set;


public class CCustomerService implements CustomerService{

    private HashMap<Integer, Customer> customers;

    private int loginCustomer;

    public CCustomerService() {
        this.customers = new HashMap<>();
    }

    @Override
    public Customer getId(Integer id) {
        return customers.get(id);
    }


    @Override
    public double getBalance(int customerId) {

        return 0;
    }


    @Override
    public void add(Customer customer) {
        customers.put(customer.getId(), customer);
    }

    @Override
    public Set<Integer> getCustomersIds() {
        return customers.keySet();
    }
}



