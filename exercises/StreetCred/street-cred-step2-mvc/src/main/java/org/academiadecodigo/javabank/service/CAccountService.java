package org.academiadecodigo.javabank.service;

import org.academiadecodigo.javabank.factories.AccountFactory;
import org.academiadecodigo.javabank.model.account.Account;
import org.academiadecodigo.javabank.model.account.AccountType;

import java.util.HashMap;
import java.util.Map;

public class CAccountService implements AccountService{

    private AccountFactory accountFactory;
    private Map<Integer, Account> accountMap;

    public CAccountService(){
        accountMap = new HashMap<>();
    }

    public Account openAccount (AccountType accounttype){
        Account newAccount = accountFactory.createAccount(accounttype);
        accountMap.put(newAccount.getId(), newAccount);
        return newAccount;
    }

    @Override
    public void add(Account account) {

    }

    public void setAccountFactory(AccountFactory accountFactory){
        this.accountFactory = accountFactory;
    }


    @Override
    public void deposit(int id, double amount) {
        accountMap.get(id).credit(amount);
    }

    @Override
    public void withdraw(int id, double amount) {
        Account account = accountMap.get(id);

        if(!account.canWithdraw()){
            return;
        }
        accountMap.get(id).debit(amount);
    }

    @Override
    public void transfer(int srcId, int dstId, double amount) {
        Account srcAccount = accountMap.get(srcId);
        Account dstAccount = accountMap.get(dstId);
        if(srcAccount.canCredit(amount) && dstAccount.canCredit(amount)){
            srcAccount.debit(amount);
            dstAccount.credit(amount);
        }
    }
}
