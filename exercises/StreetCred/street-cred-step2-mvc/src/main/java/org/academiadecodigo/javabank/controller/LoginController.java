package org.academiadecodigo.javabank.controller;


import org.academiadecodigo.javabank.view.LoginView;


import java.util.Set;

/**
 * The {@link LoginView} controller
 */
public class LoginController extends AbstractController {

    private Controller nextController;


    /**
     * Sets the next controller
     *
     * @param nextController the next controller to be set
     */
    public void setNextController(Controller nextController) {
        this.nextController = nextController;
    }

    /**
     * Identifies the logged in customer
     *
     * @param id the customer id
     */

//TODO Changed this.
    public void onLogin(int id) {
        //cAuthService
        cAuthService.setLoginCustomer(id);
        nextController.init();
    }


    /**
     * Gets set of customer ids from bank
     *
     * @return set of customer account ids
     */
    public Set<Integer> getCustomerIds() {
        return /*cAuthService*/cCustomerService.getCustomersIds();
    }




}
