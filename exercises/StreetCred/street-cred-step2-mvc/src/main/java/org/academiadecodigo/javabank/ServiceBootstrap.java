package org.academiadecodigo.javabank;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.javabank.controller.*;
import org.academiadecodigo.javabank.controller.transaction.DepositController;
import org.academiadecodigo.javabank.controller.transaction.WithdrawalController;
import org.academiadecodigo.javabank.factories.AccountFactory;
import org.academiadecodigo.javabank.model.Customer;
import org.academiadecodigo.javabank.service.CAccountService;
import org.academiadecodigo.javabank.service.CAuthService;
import org.academiadecodigo.javabank.service.CCustomerService;
import org.academiadecodigo.javabank.view.*;

import java.util.HashMap;
import java.util.Map;

public class ServiceBootstrap {


   public CCustomerService generateTestDataC(){
       CCustomerService cCustomerService = new CCustomerService();
       Customer c1 = new Customer(1, "Rui");
       Customer c2 = new Customer(2, "Sergio");
       Customer c3 = new Customer(3, "Bruno");

       cCustomerService.add(c1);
       cCustomerService.add(c2);
       cCustomerService.add(c3);

       return cCustomerService;
   }


    public LoginController wireObjects(CCustomerService cCustomerService){

       CAuthService cAuthService = new CAuthService();


        CAccountService cAccountService = new CAccountService();
        cAccountService.setAccountFactory(new AccountFactory());



        Prompt prompt = new Prompt(System.in, System.out);
        LoginController loginController = new LoginController();
        LoginView loginView = new LoginView();

        loginController.setcAuthService(cAuthService);
        loginController.setcCustomerService(cCustomerService);

        loginController.setView(loginView);
        loginView.setLoginController(loginController);
        loginView.setPrompt(prompt);


        // wire main controller and view

        MainController mainController = new MainController();
        MainView mainView = new MainView();
        mainView.setPrompt(prompt);
        mainView.setMainController(mainController);
        mainController.setView(mainView);
        mainController.setcAuthService(cAuthService);
        mainController.setcAccountService(cAccountService);
        mainController.setcCustomerService(cCustomerService);
        loginController.setNextController(mainController);




        // wire balance controller and view NOT DONE YET
        BalanceController balanceController = new BalanceController();
        BalanceView balanceView = new BalanceView();
        balanceController.setView(balanceView);
        balanceController.setcCustomerService(cCustomerService);
        balanceController.setcAuthService(cAuthService);

        balanceView.setBalanceController(balanceController);



        NewAccountView newAccountView = new NewAccountView();
        NewAccountController newAccountController = new NewAccountController();
        newAccountController.setcAccountService(cAccountService);
        newAccountController.setView(newAccountView);
        newAccountView.setNewAccountController(newAccountController);
        newAccountController.setcAuthService(cAuthService);


// wire account transactions controllers and views NOT DONE YEY
        DepositController depositController = new DepositController();
        WithdrawalController withdrawalController = new WithdrawalController();
        AccountTransactionView depositView = new AccountTransactionView();
        AccountTransactionView withdrawView = new AccountTransactionView();
        depositController.setcAccountService(cAccountService);
        depositController.setcAuthService(cAuthService);
        depositController.setView(depositView);
        withdrawalController.setcAccountService(cAccountService);
        withdrawalController.setcAuthService(cAuthService);
        withdrawalController.setView(withdrawView);
        depositView.setPrompt(prompt);
        depositView.setTransactionController(depositController);
        withdrawView.setPrompt(prompt);
        withdrawView.setTransactionController(withdrawalController);



        Map<Integer, Controller> controllerMap = new HashMap<>();
        controllerMap.put(UserOptions.GET_BALANCE.getOption(), balanceController);
        controllerMap.put(UserOptions.OPEN_ACCOUNT.getOption(), newAccountController);
        controllerMap.put(UserOptions.DEPOSIT.getOption(), depositController);
        controllerMap.put(UserOptions.WITHDRAW.getOption(), withdrawalController);
        mainController.setControllerMap(controllerMap);

        return loginController;

    }

  /*
    public CAuthService generateTestData(){
        CAuthService cAuthService = new CAuthService();



        Customer c1 = new Customer(1, "Rui");
        Customer c2 = new Customer(2, "Sergio");
        Customer c3 = new Customer(3, "Bruno");

        cAuthService.addCustomer(c1);
        cAuthService.addCustomer(c2);
        cAuthService.addCustomer(c3);

        return cAuthService;
    }

    public LoginController wireObjects(CAuthService cAuthService){

        CAccountService cAccountService = new CAccountService();
        cAccountService.setAccountFactory(new AccountFactory());

        CCustomerService cCustomerService = new CCustomerService();



        Prompt prompt = new Prompt(System.in, System.out);
        LoginController loginController = new LoginController();
        LoginView loginView = new LoginView();

        loginController.setcAuthService(cAuthService);

        loginController.setView(loginView);
        loginView.setLoginController(loginController);
        loginView.setPrompt(prompt);


        // wire main controller and view

        MainController mainController = new MainController();
        MainView mainView = new MainView();
        mainView.setPrompt(prompt);
        mainView.setMainController(mainController);
        mainController.setView(mainView);
        mainController.setcAuthService(cAuthService);
        mainController.setcAccountService(cAccountService);
        loginController.setNextController(mainController);




        // wire balance controller and view NOT DONE YET
        BalanceController balanceController = new BalanceController();
        BalanceView balanceView = new BalanceView();
        balanceController.setView(balanceView);
        balanceController.setcCustomerService(cCustomerService);
        balanceController.setcAuthService(cAuthService);

        balanceView.setBalanceController(balanceController);



        NewAccountView newAccountView = new NewAccountView();
        NewAccountController newAccountController = new NewAccountController();
        newAccountController.setcAccountService(cAccountService);
        newAccountController.setView(newAccountView);
        newAccountView.setNewAccountController(newAccountController);
        newAccountController.setcAuthService(cAuthService);


// wire account transactions controllers and views NOT DONE YEY
        DepositController depositController = new DepositController();
        WithdrawalController withdrawalController = new WithdrawalController();
        AccountTransactionView depositView = new AccountTransactionView();
        AccountTransactionView withdrawView = new AccountTransactionView();
        depositController.setcAccountService(cAccountService);
        depositController.setcAuthService(cAuthService);
        depositController.setView(depositView);
        withdrawalController.setcAccountService(cAccountService);
        withdrawalController.setcAuthService(cAuthService);
        withdrawalController.setView(withdrawView);
        depositView.setPrompt(prompt);
        depositView.setTransactionController(depositController);
        withdrawView.setPrompt(prompt);
        withdrawView.setTransactionController(withdrawalController);



        Map<Integer, Controller> controllerMap = new HashMap<>();
        controllerMap.put(UserOptions.GET_BALANCE.getOption(), balanceController);
        controllerMap.put(UserOptions.OPEN_ACCOUNT.getOption(), newAccountController);
        controllerMap.put(UserOptions.DEPOSIT.getOption(), depositController);
        controllerMap.put(UserOptions.WITHDRAW.getOption(), withdrawalController);
        mainController.setControllerMap(controllerMap);

        return loginController;

    }*/
}
