package org.academiadecodigo.javabank.controller;

import org.academiadecodigo.javabank.model.Customer;

/**
 * Common interface for controllers, provides a method for a controller to be initialized
 */
public interface Controller {

    /**
     * Initializes the controller
     */
    void init();

    /**
     * Returns accessing customer from bank
     *
     * @return accessing customer
     */
    Customer getLoginCustomer();
}
