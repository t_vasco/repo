package org.academiadecodigo.javabank.controller;

import org.academiadecodigo.javabank.model.Customer;
import org.academiadecodigo.javabank.service.CCustomerService;
import org.academiadecodigo.javabank.view.BalanceView;

/**
 * The {@link BalanceView} controller
 */
public class BalanceController extends AbstractController {

    CCustomerService cCustomerService;

    public void setcCustomerService(CCustomerService cCustomerService) {
        this.cCustomerService = cCustomerService;
    }
}
