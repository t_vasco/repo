package org.academiadecodigo.streetcred.domain;

import org.academiadecodigo.streetcred.domain.account.Account;
import org.academiadecodigo.streetcred.domain.account.AccountType;
import org.academiadecodigo.streetcred.managers.AccountManager;

import java.util.HashMap;
import java.util.Map;

/**
 * The customer domain entity
 */
public class Customer {

    //Tem as suas contas
    //Tem um account manager
    private AccountManager accountManager;
    private Map<Integer, Account> accounts = new HashMap<>();

    private int myCustomerId;

    /**
     * Sets the account manager
     *
     * @param accountManager the account manager to set
     */
    public void setAccountManager(AccountManager accountManager) {
        this.accountManager = accountManager;
    }

    /**
     * Opens a new account
     *
     * @param accountType the account type to be opened
     * @return the new account id
     * @see AccountManager#openAccount(AccountType)
     */
    public int openAccount(AccountType accountType) {
        Account account = accountManager.openAccount(accountType);
        accounts.put(account.getId(), account);
        return account.getId();
    }

    /**
     * Gets the balance of an {@link Account}
     *
     * @param id the id of the account
     * @return the account balance
     */
    public double getBalance(int id) {
        return accounts.get(id).getBalance();
    }

    /**
     * Gets the total customer balance
     *
     * @return the customer balance
     */
    public double getBalance() {

        double balance = 0;

        for (Account account : accounts.values()) {
            balance += account.getBalance();
        }

        return balance;
    }

    public int getMyCustomerId() {
        return myCustomerId;
    }

    public void setCustomerId(int customerId) {
        this.myCustomerId = customerId;
    }
    public boolean hasAccounts(){

        return !accounts.isEmpty();
    }
}
