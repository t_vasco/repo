package org.academiadecodigo.streetcred;

import org.academiadecodigo.streetcred.domain.Bank;
import org.academiadecodigo.streetcred.domain.Customer;
import org.academiadecodigo.streetcred.domain.account.AccountType;
import org.academiadecodigo.streetcred.managers.AccountManager;

public class Main {




    public static void main(String[] args) {

        AccountManager accMng1 = new AccountManager();

        Customer customer1 = new Customer();

        Bank bank1 = new Bank(accMng1);

        bank1.addCustomer(customer1);

        customer1.setAccountManager(accMng1);
        //customer1.openAccount(AccountType.CHECKING);

        bank1.startApp();
    }
}
