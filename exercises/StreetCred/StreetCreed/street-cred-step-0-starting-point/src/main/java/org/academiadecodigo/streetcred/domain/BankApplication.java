package org.academiadecodigo.streetcred.domain;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.integer.IntegerInputScanner;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;

import java.util.logging.Level;
import java.util.logging.Logger;


public class BankApplication {

    private Bank bank;
    private int customerNum;
    private String[] appOptions = new String[5];

    private Prompt prompt = new Prompt(System.in, System.out);
    private Logger logger = Logger.getLogger(BankApplication.class.getName());
    private MenuInputScanner menuInputScanner;


    public BankApplication(Bank bank){
        this.bank = bank;
        this.customerNum = -1;
        appOptions[0] = "How's my rep";
        appOptions[1] = "Make a deposit";
        appOptions[2] = "Make Withdrawal";
        appOptions[3] = "Open Account";
        appOptions[4] = "Quit";
    }


    public void askUserNum(){

        try {
            IntegerInputScanner question = new IntegerInputScanner();
            question.setMessage("\nWhat is your customer number?");

            while (customerNum <= 0) {
                Integer answer = prompt.getUserInput(question);
                if (answer > 0) {
                    customerNum = answer;
                    System.out.println("User number inserted: " + customerNum);
                    checkLogin(customerNum);
                }
            }
        } catch (NumberFormatException ex) {
            logger.log(Level.INFO, "Please Insert a number");
        }

    }



    private void checkLogin(int customerNum){
        if(customerNum > bank.getNumClients()){
            logger.log(Level.INFO, "Your account does not exist");
            return;
        }
        System.out.println("You are logged in");
        boolean hasAccount = userHasAccount(customerNum);

        optionMenu(hasAccount);
        logger.log(Level.FINE, "User: " + customerNum + " logged in.");
    }


    public void optionMenu(boolean hasAccount){
        menuInputScanner = new MenuInputScanner(appOptions);
        menuInputScanner.setMessage("Welcome to your ");
        if(!hasAccount){
            System.out.println("You do not have an account, please create an account before you proceed.");
        }
        Integer answer = prompt.getUserInput(menuInputScanner);
        optionSelected(answer);
    }



    public boolean userHasAccount(int id){
        if(bank.customerHasAccount(id)){
            return true;
        }
        return false;
    }


    public void optionSelected(int answer){

        boolean online = true;

        while (online) {
            switch (answer) {
                case 1:
                    System.out.println("Your Rep");
                    break;
                case 2:
                    System.out.println("you made a deposit");
                    break;
                case 3:
                    System.out.println("You made an withdrawal");
                case 4:
                    System.out.println("You created an account");
                    break;
                case 5:
                    online = false;
                    break;
            }
        }

    }

}
