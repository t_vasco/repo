package org.academiadecodigo.streetcred.domain;

import org.academiadecodigo.streetcred.managers.AccountManager;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * The bank entity
 */
public class Bank {

    private AccountManager accountManager;
    private int numClients;
    private BankApplication bankApp;

    private Map<Integer, Customer> customers = new HashMap<>();


    /**
     * Creates a new instance of Bank and initializes it with the given account manager
     *
     * @param accountManager the account manager
     */
    public Bank(AccountManager accountManager) {
        this.accountManager = accountManager;
        this.numClients = 0;
        this.bankApp = new BankApplication(this);
    }

    /**
     * Adds a new customer to the bank
     *
     * @param customer the new bank customer
     * @see Customer#setAccountManager(AccountManager)
     */
    //Tenho o número do customer conforme o número de adição.
    //Se tiver um ID, quem define o ID é o banco.

    public void addCustomer(Customer customer) {
        numClients++;
        customer.setCustomerId(numClients);
        customers.put(numClients, customer);
        customer.setAccountManager(accountManager);

    }

    public Customer getCustomer(int id) {
        return customers.get(id);
    }

    /**
     * Gets the total balance of the bank
     *
     * @return the bank total balance
     */
    public double getBalance() {

        double balance = 0;

        for (Customer customer : customers.values()) {
            balance += customer.getBalance();
        }
        return balance;
    }

    public void startApp(){
        bankApp.askUserNum();
    }

    public int getNumClients() {
        return numClients;
    }

    public boolean customerHasAccount(int customerId){
        Customer currentCustomer = customers.get(customerId);

        System.out.println("bank Customer has Account: " + currentCustomer.hasAccounts() );

        return currentCustomer.hasAccounts();
    }
}
