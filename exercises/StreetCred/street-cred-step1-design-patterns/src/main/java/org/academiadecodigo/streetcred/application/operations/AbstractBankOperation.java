package org.academiadecodigo.streetcred.application.operations;

import org.academiadecodigo.streetcred.application.BankApplication;
import org.academiadecodigo.streetcred.controller.MenuController;
import org.academiadecodigo.streetcred.domain.Customer;

/**
 * A generic bank operation to be used as a base for concrete types of bank operations
 * @see Operation
 */
public abstract class AbstractBankOperation implements Operation {

    protected BankApplication bankApplication;
    protected Customer customer;
    protected MenuController menuController;

    //TODO DAR UM MENU CONTROLLER

    public AbstractBankOperation(MenuController menuController) {
        this.menuController = menuController;
        customer = menuController.getBank().getCustomer(menuController.getAccessingCustomerId());
    }

    /**
     * Initializes a new {@code AbstractBankOperation} given a bank application
     *
     * @param bankApplication the bank application
     */
   /* public AbstractBankOperation(BankApplication bankApplication) {
        this.bankApplication = bankApplication;
        customer = bankApplication.getBank().getCustomer(bankApplication.getAccessingCustomerId());
    }*/
}
