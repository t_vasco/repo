package org.academiadecodigo.streetcred.view;

import org.academiadecodigo.bootcamp.scanners.integer.IntegerSetInputScanner;
import org.academiadecodigo.streetcred.application.Messages;

import java.util.Set;

public class LoginView extends AbstractView  {


    private Set<Integer> numberID;

    private int userInput;


    public void setNumberID(Set<Integer> numberID) {
        this.numberID = numberID;
    }

    @Override
    public void show() {
        IntegerSetInputScanner scanner = new IntegerSetInputScanner(numberID);
        scanner.setMessage(Messages.CHOOSE_CUSTOMER);
        scanner.setError(Messages.ERROR_INVALID_CUSTOMER);

        userInput =  prompt.getUserInput(scanner);
    }

    public int getUserInput() {
        return userInput;
    }
}
