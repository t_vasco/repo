package org.academiadecodigo.streetcred.application.operations.transaction;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.integer.IntegerSetInputScanner;
import org.academiadecodigo.bootcamp.scanners.precisiondouble.DoubleInputScanner;
import org.academiadecodigo.streetcred.application.BankApplication;
import org.academiadecodigo.streetcred.application.Messages;
import org.academiadecodigo.streetcred.application.operations.AbstractBankOperation;
import org.academiadecodigo.streetcred.controller.MenuController;
import org.academiadecodigo.streetcred.managers.AccountManager;

/**
 * A generic account transaction to be used as a base for concrete transaction implementations
 */
public abstract class AbstractAccountTransactionOperation extends AbstractBankOperation {

    protected AccountManager accountManager;
    private Prompt prompt;

    /**
     * Initializes a new {@code AbstractAccountTransactionOperation} given a bank application
     *
     * @see AbstractBankOperation#AbstractBankOperation(BankApplication)
     */

    //TODO GIVE HIM A MENU CONTROLLER
  /*  public AbstractAccountTransactionOperation(BankApplication bankApplication) {
        super(bankApplication);
        prompt = bankApplication.getPrompt();
        accountManager = bankApplication.getBank().getAccountManager();
    }*/

    public AbstractAccountTransactionOperation(MenuController menuController) {
        super(menuController);
        prompt = menuController.getPrompt();
        accountManager = menuController.getBank().getAccountManager();
    }

    /**
     * Performs the transaction operation
     *
     * @see AbstractBankOperation#execute()
     * @see AbstractAccountTransactionOperation#hasAccounts()
     */
    @Override
    public void execute() {

        if (!hasAccounts()) {
            System.out.println("\n" + Messages.ERROR_NO_ACCOUNT);
            return;
        }

        System.out.println("\n" + Messages.OPEN_ACCOUNTS + buildAccountList());
    }

    /**
     * Checks if customer has accounts
     *
     * @return {@code true} if the customer has accounts
     */
    protected boolean hasAccounts() {
        return customer.getAccountIds().size() > 0;
    }

    /**
     * Shows all the customer accounts
     *
     * @return the customer accounts
     */
    private String buildAccountList() {

        StringBuilder builder = new StringBuilder();

        for (Integer id : customer.getAccountIds()) {
            builder.append(id);
            builder.append(" ");
        }

        return builder.toString();
    }

    /**
     * Prompts the user for an account number
     *
     * @return the account id
     */
    protected int scanAccount() {
        IntegerSetInputScanner scanner = new IntegerSetInputScanner(customer.getAccountIds());
        scanner.setMessage(Messages.CHOOSE_ACCOUNT);
        scanner.setError(Messages.ERROR_INVALID_ACCOUNT);

        return prompt.getUserInput(scanner);
    }

    /**
     * Prompts the user for a transaction amount
     *
     * @return the amount to be transactioned
     */
    protected double scanAmount() {
        DoubleInputScanner scanner = new DoubleInputScanner();
        scanner.setMessage(Messages.CHOOSE_AMOUNT);
        scanner.setError(Messages.ERROR_INVALID_AMOUNT);

        return prompt.getUserInput(scanner);
    }
}
