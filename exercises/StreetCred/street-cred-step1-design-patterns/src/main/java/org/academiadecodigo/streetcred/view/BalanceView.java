package org.academiadecodigo.streetcred.view;

import org.academiadecodigo.streetcred.application.Messages;
import org.academiadecodigo.streetcred.controller.BalanceController;
import org.academiadecodigo.streetcred.controller.Controller;
import org.academiadecodigo.streetcred.controller.MenuController;
import org.academiadecodigo.streetcred.domain.account.Account;

import java.text.DecimalFormat;
import java.util.Set;

public class BalanceView extends AbstractView {

    private DecimalFormat df = new DecimalFormat("#.###");

    private BalanceController balanceController;



    public void show() {


        System.out.println("\n" + balanceController.getBank().getCustomer().getName() + Messages.BALANCE_MESSAGE + "\n");


        Set<Account> accounts =  balanceController.getBank().getCustomer().getAccounts();
        for (Account account : accounts) {
            System.out.println(account.getId() + "\t" + account.getAccountType() + "\t" + df.format(account.getBalance()));
        }

        System.out.println("\n\n" + Messages.BALANCE_TOTAL_MESSAGE + df.format(balanceController.getBank().getCustomer().getBalance()));
    }

    public void setController(BalanceController controller) {
        this.balanceController = controller;
    }
}
