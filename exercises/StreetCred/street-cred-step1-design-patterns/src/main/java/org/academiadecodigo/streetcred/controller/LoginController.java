package org.academiadecodigo.streetcred.controller;

import org.academiadecodigo.streetcred.domain.Bank;
import org.academiadecodigo.streetcred.view.LoginView;
import org.academiadecodigo.streetcred.view.View;


public class LoginController extends AbstractController {

    private LoginView view;
    private Bank bank;


    @Override
    public void init() {
        view.setNumberID(bank.getCustomerIds());
        this.view.show();
        //view.getUserInput();

        bank.setAccessingCustomer(view.getUserInput());
        nextControl.init();

    }


    public void setView(LoginView view) {
        this.view = view;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }


}
