package org.academiadecodigo.streetcred.application.operations.transaction;

import org.academiadecodigo.streetcred.application.BankApplication;
import org.academiadecodigo.streetcred.application.UserOptions;
import org.academiadecodigo.streetcred.controller.MenuController;

/**
 * An account transaction used to deposit an amount
 * @see AbstractAccountTransactionOperation
 * @see UserOptions#DEPOSIT
 */
public class DepositOperation extends AbstractAccountTransactionOperation {

    /**
     * Initializes a new {@code DepositOperation}
     *
     * @see AbstractAccountTransactionOperation
     */
    //TODO give him a Menu Controller
   /* public DepositOperation(BankApplication bankApplication) {
        super(bankApplication);
    }*/

    public DepositOperation(MenuController menuController) {
        super(menuController);
    }

    /**
     * Deposit an amount into an account
     *
     * @see AbstractAccountTransactionOperation#execute()
     */
    @Override
    public void execute() {

        super.execute();

        if (!hasAccounts()) {
            return;
        }

        Integer accountId = scanAccount();
        Double amount = scanAmount();

        if (customer.getAccountIds().contains(accountId)) {
            accountManager.deposit(accountId, amount);
        }
    }
}
