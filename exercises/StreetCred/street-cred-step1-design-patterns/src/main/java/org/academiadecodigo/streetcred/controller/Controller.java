package org.academiadecodigo.streetcred.controller;

import org.academiadecodigo.streetcred.view.View;

public interface Controller {

     void init();

}

