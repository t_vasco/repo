package org.academiadecodigo.streetcred;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.streetcred.application.UserOptions;
import org.academiadecodigo.streetcred.controller.BalanceController;
import org.academiadecodigo.streetcred.controller.Controller;
import org.academiadecodigo.streetcred.controller.LoginController;
import org.academiadecodigo.streetcred.controller.MenuController;
import org.academiadecodigo.streetcred.domain.Bank;
import org.academiadecodigo.streetcred.domain.Customer;
import org.academiadecodigo.streetcred.managers.AccountManager;
import org.academiadecodigo.streetcred.view.BalanceView;
import org.academiadecodigo.streetcred.view.LoginView;
import org.academiadecodigo.streetcred.view.MenuView;

import java.util.HashMap;
import java.util.Map;


public class ProxyApp {


    public static void main(String[] args) {

        Bank bank = new Bank();
        AccountManager accountManager = new AccountManager();
        bank.setAccountManager(accountManager);

        Customer c1 = new Customer(1, "Rui");
        Customer c2 = new Customer(2, "Sergio");
        Customer c3 = new Customer(3, "Bruno");
        bank.addCustomer(c1);
        bank.addCustomer(c2);
        bank.addCustomer(c3);



        Prompt prompt = new Prompt(System.in, System.out);

        //Login
        LoginView loginView = new LoginView();
        loginView.setPrompt(prompt);

        LoginController loginController = new LoginController();
        loginController.setBank(bank);
        loginController.setView(loginView);
        loginController.setBank(bank);



        //Menu

        MenuView menuView = new MenuView();
        menuView.setPrompt(prompt);
        MenuController menuController = new MenuController();
        menuController.setView(menuView);
        loginController.setNextControl(menuController);
        menuController.setBank(bank);


        //Balance Controller
        BalanceController balanceController = new BalanceController();
        BalanceView balanceView = new BalanceView();
        balanceView.setPrompt(prompt);
        balanceView.setController(balanceController);

        balanceController.setView(balanceView);
        balanceController.setBank(bank);





        Map<Integer, Controller> operationsMap = new HashMap<>();
        operationsMap.put(UserOptions.GET_BALANCE.getOption(), balanceController);

        menuController.setControllerMap(operationsMap);
        loginController.init();
    }
}
