package org.academiadecodigo.streetcred.view;

import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.streetcred.application.Messages;
import org.academiadecodigo.streetcred.application.UserOptions;

public class MenuView extends AbstractView {

    private int userChoice;


    @Override
    public void show() {
        MenuInputScanner mainMenu = new MenuInputScanner(UserOptions.getMessages());
        mainMenu.setError(Messages.ERROR_INVALID_OPTION);
        mainMenu.setMessage(Messages.MENU_WELCOME);

        userChoice = prompt.getUserInput(mainMenu);

    }

    public int getUserChoice() {
        return userChoice;
    }

}