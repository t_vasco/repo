package org.academiadecodigo.streetcred.application.operations.transaction;

import org.academiadecodigo.streetcred.application.BankApplication;
import org.academiadecodigo.streetcred.application.UserOptions;
import org.academiadecodigo.streetcred.controller.MenuController;

/**
 * An account transaction used to withdraw an amount
 * @see AbstractAccountTransactionOperation
 * @see UserOptions#WITHDRAW
 */
public class WithdrawOperation extends AbstractAccountTransactionOperation {

    /**
     * Initializes a new {@code WithdrawOperation}
     *
     * @see AbstractAccountTransactionOperation
     */
    //TODO GIVE HIM A MENU CONTROLLER INSTEAD
   /* public WithdrawOperation(BankApplication bankApplication) {
        super(bankApplication);
    }*/

    public WithdrawOperation(MenuController menuController) {
        super(menuController);
    }

    /**
     * Withdraw an amount from an account
     *
     * @see AbstractAccountTransactionOperation#execute()
     */
    @Override
    public void execute() {

        super.execute();

        if (!hasAccounts()) {
            return;
        }

        Integer accountId = scanAccount();
        Double amount = scanAmount();

        if (customer.getAccountIds().contains(accountId)) {
            accountManager.withdraw(accountId, amount);
        }
    }
}
