package org.academiadecodigo.streetcred.controller;

import org.academiadecodigo.streetcred.view.View;

public abstract class AbstractController implements Controller {

    protected View view;
    protected Controller nextControl;

    public void setNextControl(Controller nextControl) {
        this.nextControl = nextControl;
    }

    public void setView(View view) {
        this.view = view;
    }

    @Override
    public void init() {

        view.show();

    }



}
