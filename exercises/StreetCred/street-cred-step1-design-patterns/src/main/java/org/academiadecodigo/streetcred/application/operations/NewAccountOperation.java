package org.academiadecodigo.streetcred.application.operations;

import org.academiadecodigo.streetcred.application.BankApplication;
import org.academiadecodigo.streetcred.application.Messages;
import org.academiadecodigo.streetcred.application.UserOptions;
import org.academiadecodigo.streetcred.controller.MenuController;
import org.academiadecodigo.streetcred.domain.account.AccountType;

/**
 * A bank operation to create new accounts
 * @see AbstractBankOperation
 * @see UserOptions#OPEN_ACCOUNT
 */
public class NewAccountOperation extends AbstractBankOperation {

    /**
     * Creates a new {@code NewAccountOperation}
     *
     * @see AbstractBankOperation#AbstractBankOperation(BankApplication)
     */

    //TODO dar um menu controller;
     /*  public NewAccountOperation(BankApplication bankApplication) {
        super(bankApplication);
    }*/


    public NewAccountOperation(MenuController menuController) {
        super(menuController);
    }

    /**
     * Creates a new bank account
     *
     * @see AbstractBankOperation#execute()
     */
    @Override
    public void execute() {

        int accountId = customer.openAccount(AccountType.CHECKING);

        System.out.println("\n" + Messages.CREATED_ACCOUNT + customer.getName() + " : " + accountId);
    }
}
