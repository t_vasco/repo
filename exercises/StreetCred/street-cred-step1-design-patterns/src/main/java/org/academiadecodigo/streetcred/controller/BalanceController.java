package org.academiadecodigo.streetcred.controller;

import org.academiadecodigo.streetcred.domain.Bank;
import org.academiadecodigo.streetcred.domain.Customer;




public class BalanceController extends AbstractController{

    protected Customer customer;
    protected Bank bank;


    public Customer getCustomer() {
        return customer;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }


    public Bank getBank() {
        return bank;
    }
}
