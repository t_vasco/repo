package org.academiadecodigo.streetcred.application;

/**
 * Messages to be used throughout the application
 */
public class Messages {

    public final static String MENU_WELCOME = "Welcome to $treet Cred";
    public final static String MENU_OPEN_ACCOUNT = "Open Account";
    public final static String MENU_DEPOSIT = "Make Deposit";
    public final static String MENU_WITHDRAW = "Make Withdrawal";
    public final static String MENU_GET_BALANCE = "How's my balance?";
    public final static String MENU_QUIT = "Quit";

    public final static String OPEN_ACCOUNTS = "You have the following accounts: ";
    public final static String CREATED_ACCOUNT = "Created account for ";
    public final static String CHOOSE_ACCOUNT = "Choose account: ";
    public final static String CHOOSE_AMOUNT = "Please insert amount: ";
    public final static String CHOOSE_CUSTOMER = "What's your number, huh player?: ";
    public final static String BALANCE_MESSAGE = " balance ";
    public final static String BALANCE_TOTAL_MESSAGE = "Total Balance: ";

    public final static String ERROR_INVALID_OPTION = "That is an invalid option";
    public final static String ERROR_INVALID_CUSTOMER = "You do not seem to be a valid customer";
    public final static String ERROR_NO_ACCOUNT = "Open a bank account first, please!";
    public final static String ERROR_INVALID_ACCOUNT = "That is an invalid account!";
    public final static String ERROR_INVALID_AMOUNT = "That is an invalid amount!";

}
