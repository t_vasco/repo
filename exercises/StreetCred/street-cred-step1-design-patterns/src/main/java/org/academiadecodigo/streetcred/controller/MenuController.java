package org.academiadecodigo.streetcred.controller;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.streetcred.application.UserOptions;
import org.academiadecodigo.streetcred.application.operations.BalanceOperation;
import org.academiadecodigo.streetcred.application.operations.NewAccountOperation;
import org.academiadecodigo.streetcred.application.operations.Operation;
import org.academiadecodigo.streetcred.application.operations.transaction.DepositOperation;
import org.academiadecodigo.streetcred.application.operations.transaction.WithdrawOperation;
import org.academiadecodigo.streetcred.domain.Bank;
import org.academiadecodigo.streetcred.domain.Customer;
import org.academiadecodigo.streetcred.view.MenuView;

import java.util.HashMap;
import java.util.Map;

public class MenuController extends AbstractController {


    private MenuView menuView;
    private Map<Integer, Controller> controllerMap;
    private Bank bank;


    private void menuLoop() {

        int userChoice = view.getUserChoice();

        if (userChoice == UserOptions.QUIT.getOption()) {
            System.out.println("teste");
            return;
        }


        controllerMap.get(userChoice).init();

        menuLoop();

    }

   /* private Map<Integer, Operation> buildOperationsMap(){
        Map<Integer, Operation> map = new HashMap<>();
        map.put(UserOptions.GET_BALANCE.getOption(), new BalanceOperation(this));
        map.put(UserOptions.DEPOSIT.getOption(), new DepositOperation(this));
        map.put(UserOptions.WITHDRAW.getOption(), new WithdrawOperation(this));
        map.put(UserOptions.OPEN_ACCOUNT.getOption(), new NewAccountOperation(this));

        return map;
    }*/
   protected Customer customer;



    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    @Override
    public void init() {

        int accessingCustomerId = menuView.getUserChoice();
        bank.setAccessingCustomer(accessingCustomerId);
        customer = bank.getCustomer();

        super.init();

        menuLoop();
    }

   /* public void setAccessingCustomerId(int accessingCustomerId) {
        this.accessingCustomerId = accessingCustomerId;
    }*/

   public Prompt getPrompt(){
        return menuView.getPrompt();
   }

    public Customer getCustomer() {
        return customer;
    }

    public void setControllerMap(Map<Integer, Controller> controllerMap) {
        this.controllerMap = controllerMap;
    }
}
