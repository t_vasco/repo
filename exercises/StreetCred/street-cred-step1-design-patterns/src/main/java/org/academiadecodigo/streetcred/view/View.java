package org.academiadecodigo.streetcred.view;

import org.academiadecodigo.bootcamp.Prompt;

public interface View {

    void show();

    void setPrompt(Prompt promt);

}
