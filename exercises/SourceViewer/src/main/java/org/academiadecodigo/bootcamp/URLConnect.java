package org.academiadecodigo.bootcamp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class URLConnect {

    public static void main(String[] args) {
       try{
           URL reader = new URL("http://www.stallman.com");
           URLConnection urlConnection = reader.openConnection();
           BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

           String inputLine;
           while((inputLine = in.readLine()) != null)
               System.out.println(inputLine);
           in.close();

       }catch (MalformedURLException ex){
           System.err.println("The URL is Wrong: " + ex);
       }catch (IOException ex){
           System.err.println("IO Exeption: " + ex);
       }

    }

}
