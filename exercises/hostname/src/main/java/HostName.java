import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

public class HostName {

    private static String hostName = "www.stallman.com" ;


    public static void main(String[] args)throws Exception {
        try {
            InetAddress is = InetAddress.getByName(hostName);
            System.out.println("Name " + is.getHostName());
            System.out.println("Address " + is.getHostAddress());
            System.out.println("Reach: " + is.isReachable(1000));
            System.out.println("Canon " + is.getCanonicalHostName());
            System.out.println("Hash " + is.hashCode());
            System.out.println("To String " + is.toString());
            System.out.println("Loopback " + is.getLoopbackAddress());
            System.out.println("Multi Cast " + is.isMulticastAddress());
            System.out.println("is any " + is.isAnyLocalAddress());
            System.out.println("is loopback " + is.isLoopbackAddress());
            System.out.println("Is Site Local " + is.isSiteLocalAddress());
            System.out.println("is Mc " + is.isMCGlobal());
            System.out.println("Is McNode "+ is.isMCNodeLocal());
            System.out.println("Is McSite Local " + is.isMCSiteLocal());
            System.out.println("Is Mc Org Local " + is.isMCOrgLocal());
            System.out.println("");
        }
        catch (Exception ex){
            System.out.println(ex);
        }

        try{
            DatagramSocket dt = new DatagramSocket();
            String str = "I am Here, are you there?";
            InetAddress ip = InetAddress.getByName(hostName);

        }
        catch (Exception ex1){
            System.out.println(ex1);
        }


    }




}
