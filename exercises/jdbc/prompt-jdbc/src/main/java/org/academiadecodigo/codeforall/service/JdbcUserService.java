package org.academiadecodigo.codeforall.service;

import org.academiadecodigo.codeforall.model.User;
import org.academiadecodigo.codeforall.persistence.ConnectionManager;
import org.academiadecodigo.codeforall.utils.Security;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class JdbcUserService implements UserService {

    private Connection dbConnection;
    private ConnectionManager connectionManager;

    public JdbcUserService(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
        this.dbConnection = connectionManager.getConnection();
    }

    private void checkConnection() throws SQLException {

        if (dbConnection == null || dbConnection.isClosed()) {
            dbConnection = connectionManager.getConnection();
        }

        if (dbConnection == null) {
            throw new IllegalStateException("Error connecting to the database");
        }

    }


    /**
     * Authenticates the user using the given username and password
     *
     * @param username the user name
     * @param password the user password
     * @return true if authenticated
     */
    @Override
    public boolean authenticate(String username, String password) {
        boolean result = false;
        PreparedStatement statement = null;
        try {

            checkConnection();

            // create a query
            String query = "SELECT * FROM user WHERE username=? AND password=?;";

            statement = dbConnection.prepareStatement(query);

            statement.setString(1, username);
            statement.setString(2, Security.getHash(password));
            // execute the query
            ResultSet resultSet = statement.executeQuery();

            // if the user exists
            if (resultSet.next()) {
                result = true;
            }

        } catch (SQLException ex) {
            System.out.println("Database error: " + ex.getMessage());
        } finally {
            closeStatement(statement);
        }

        return result;
    }

    /**
     * Adds a new user
     *
     * @param user the new user to add
     */
    @Override
    public void add(User user) {

        PreparedStatement statement = null;

        try {

            checkConnection();

            if (findByName(user.getUsername()) != null) {
                return;
            }


            // create a new statement


            String query = "INSERT INTO user(username, password, email, firstname, lastname, phone)" +
                    "VALUES (?, ?, ?, ?, ?, ?)";

            statement = dbConnection.prepareStatement(query);

            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getFirstName());
            statement.setString(5, user.getLastName());
            statement.setString(6, user.getPhone());

            // execute the query
            statement.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Database error: " + ex.getMessage());
        } finally {
            closeStatement(statement);
        }

    }

    /**
     * Finds a user by name
     *
     * @param username the user name used to lookup a user
     * @return a new User if found, null otherwise
     */
    @Override
    public User findByName(String username) {

        PreparedStatement statement = null;
        User user = null;

        try {

            checkConnection();


            // create a query
            String query = "SELECT * FROM user WHERE username=?";

            statement = dbConnection.prepareStatement(query);

            statement.setString(1, username);

            // execute the query
            ResultSet resultSet = statement.executeQuery();

            // user exists
            if (resultSet.next()) {

                String usernameValue = resultSet.getString("username");
                String passwordValue = resultSet.getString("password");
                String emailValue = resultSet.getString("email");
                String firstNameValue = resultSet.getString("firstname");
                String lastNameValue = resultSet.getString("lastname");
                String phoneValue = resultSet.getString("phone");

                user = new User(usernameValue, emailValue, passwordValue, firstNameValue, lastNameValue, phoneValue);
            }

        } catch (SQLException ex) {
            System.out.println("Database error: " + ex.getMessage());
        } finally {
            closeStatement(statement);
        }

        return user;
    }


    /**
     * Finds all users
     *
     * @return list of users if found, null otherwise
     */
    @Override
    public List<User> findAll() {

        PreparedStatement statement = null;
        List<User> users = new LinkedList<>();

        try {

            checkConnection();

            // create a query
            String query = "SELECT * FROM user";

            statement = dbConnection.prepareStatement(query);

            // execute the query
            ResultSet resultSet = statement.executeQuery();

            // user exists
            while (resultSet.next()) {

                String usernameValue = resultSet.getString("username");
                String passwordValue = resultSet.getString("password");
                String emailValue = resultSet.getString("email");
                String firstNameValue = resultSet.getString("firstname");
                String lastNameValue = resultSet.getString("lastname");
                String phoneValue = resultSet.getString("phone");

                users.add(new User(usernameValue, emailValue, passwordValue, firstNameValue, lastNameValue, phoneValue));
            }

        } catch (SQLException ex) {
            System.out.println("Database error: " + ex.getMessage());
        } finally {
            closeStatement(statement);
        }

        return users;
    }


    /**
     * Count the number of existing users
     *
     * @return the number of users
     */
    @Override
    public int count() {

        PreparedStatement statement = null;
        int result = 0;

        try {

            checkConnection();

            // create a new statement

            // create a query
            String query = "SELECT COUNT(*) FROM user";

            statement = dbConnection.prepareStatement(query);


            // execute the query
            ResultSet resultSet = statement.executeQuery();

            // get the results
            if (resultSet.next()) {
                result = resultSet.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println("Database error: " + ex.getMessage());
        } finally {
            closeStatement(statement);
        }

        return result;

    }

    private void closeStatement(Statement statement) {

        if (statement == null) {
            return;
        }

        try {
            statement.close();
        } catch (SQLException ex) {
            System.out.println("Error releasing DB resources: " + ex.getMessage());
        }
    }
}

