package org.academiadecodigo.codeforall.view;

public interface View {

    void show();
}
