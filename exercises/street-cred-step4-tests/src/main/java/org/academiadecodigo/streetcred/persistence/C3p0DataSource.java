package org.academiadecodigo.streetcred.persistence;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;

public class C3p0DataSource {

    private final String dbURL = "jdbc:mysql://localhost:3306/street_cred?serverTimezone=UTC";
    private final String driverClass = "com.mysql.cj.jdbc.Driver";
    private final String user = "root";
    private final String password = "";
    private ComboPooledDataSource cpds;


    public void start() {

        cpds = new ComboPooledDataSource();

        cpds.setInitialPoolSize(1);
        cpds.setMaxPoolSize(2);
        cpds.setMinPoolSize(1);
        cpds.setAcquireIncrement(1);
        cpds.setMaxStatements(0);

        cpds.setIdleConnectionTestPeriod(300);
        try {
            cpds.setDriverClass(driverClass);
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }
        cpds.setJdbcUrl(dbURL);
        cpds.setUser(user);
        cpds.setPassword(password);


    }

    public Connection getConnection() {

        Connection connection = null;
        try {
            connection = cpds.getConnection();

        }catch (SQLException e){
            e.printStackTrace();
        }
        return connection;
    }


}
