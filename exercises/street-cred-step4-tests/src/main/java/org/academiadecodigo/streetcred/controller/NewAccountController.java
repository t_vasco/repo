package org.academiadecodigo.streetcred.controller;

import org.academiadecodigo.streetcred.factories.AccountFactory;
import org.academiadecodigo.streetcred.model.account.Account;
import org.academiadecodigo.streetcred.model.account.AccountType;
import org.academiadecodigo.streetcred.services.AccountService;
import org.academiadecodigo.streetcred.view.NewAccountView;

/**
 * The {@link NewAccountView} controller
 */
public class NewAccountController extends AbstractController {

    private AccountService accountService;

    /**
     * Sets the account service
     *
     * @param accountService the account service to set
     */
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    public Integer createAccount(AccountType accountType) {

        Account newAccount = AccountFactory.createAccount(accountType);

        accountService.add(newAccount, authService.getAccessingCustomer().getId());
        authService.getAccessingCustomer().addAccount(newAccount);

        return newAccount.getId();
    }
}
