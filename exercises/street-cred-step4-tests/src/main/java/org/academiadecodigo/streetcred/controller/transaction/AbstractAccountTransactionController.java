package org.academiadecodigo.streetcred.controller.transaction;

import org.academiadecodigo.streetcred.controller.AbstractController;
import org.academiadecodigo.streetcred.services.AccountService;
import org.academiadecodigo.streetcred.services.CustomerService;

import java.util.Set;

/**
 * A generic account transaction controller to be used as a base for concrete transaction controller implementations
 * @see AbstractController
 * @see AccountTransactionController
 */
public abstract class AbstractAccountTransactionController extends AbstractController implements AccountTransactionController {

    protected AccountService accountService;
    private CustomerService customerService;

    /**
     * Sets the account service
     *
     * @param accountService the account service to set
     */
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * Sets the customer service
     *
     * @param customerService the customer service to set
     */
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    /**
     * @see AccountTransactionController#getAccountIds()
     */
    public Set<Integer> getAccountIds() {
        return customerService.listCustomerAccountIds(authService.getAccessingCustomer().getId());
    }
}
