package org.academiadecodigo.streetcred;

import org.academiadecodigo.streetcred.controller.Controller;
import org.academiadecodigo.streetcred.persistence.C3p0DataSource;
import org.academiadecodigo.streetcred.persistence.SessionManager;
import org.academiadecodigo.streetcred.services.AccountService;
import org.academiadecodigo.streetcred.services.AuthServiceImpl;
import org.academiadecodigo.streetcred.services.jdbc.JdbcAccountService;
import org.academiadecodigo.streetcred.services.jdbc.JdbcCustomerService;
import org.academiadecodigo.streetcred.services.mock.MockAccountService;
import org.academiadecodigo.streetcred.services.mock.MockCustomerService;

public class App {

    public static void main(String[] args) {

        App app = new App();
        app.bootStrap();
    }

    private void bootStrap() {

        C3p0DataSource c3p0DataSource = new C3p0DataSource();
        c3p0DataSource.start();
        SessionManager sessionManager = new SessionManager(c3p0DataSource);

        JdbcAccountService jdbcAccountService = new JdbcAccountService(sessionManager);

        AuthServiceImpl authService = new AuthServiceImpl();
        //AccountService jdbcAccountService = new MockAccountService();
        JdbcCustomerService jdbcCustomerService = new JdbcCustomerService(jdbcAccountService, sessionManager);

        Bootstrap bootstrap = new Bootstrap();
        bootstrap.setAuthService(authService);
        bootstrap.setAccountService(jdbcAccountService);
        bootstrap.setCustomerService(jdbcCustomerService);

        Controller controller = bootstrap.wireObjects();

        // start application
        controller.init();
    }
}
