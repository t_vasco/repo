package org.academiadecodigo.streetcred.factories;

import org.academiadecodigo.streetcred.model.account.Account;
import org.academiadecodigo.streetcred.model.account.AccountType;
import org.academiadecodigo.streetcred.model.account.CheckingAccount;
import org.academiadecodigo.streetcred.model.account.SavingsAccount;

/**
 * A factory for creating accounts of different types
 */
public class AccountFactory {

    /**
     * Creates a new {@link Account}
     *
     * @param accountType the account type
     * @return the new account
     */
    public static Account createAccount(AccountType accountType) {

        Account newAccount;
        switch (accountType) {
            case CHECKING:
                newAccount = new CheckingAccount();
                break;
            case SAVINGS:
                newAccount = new SavingsAccount();
                break;
            default:
                newAccount = null;

        }

        return newAccount;
    }
}
