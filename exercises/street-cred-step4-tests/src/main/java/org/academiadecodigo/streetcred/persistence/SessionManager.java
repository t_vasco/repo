package org.academiadecodigo.streetcred.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class SessionManager {

    private C3p0DataSource c3p0DataSource;
    private Connection connection;

    public SessionManager(C3p0DataSource c3p0DataSource) {
        this.c3p0DataSource = c3p0DataSource;
    }

    public void beginRead() {
        startSession();
    }

    public void beginWrite() {
        try {
            getCurrentSession().setAutoCommit(false);
        } catch (SQLException e) {
            System.out.println("Database error: " + e.getMessage());
        }
    }

    public void commit() throws SQLException {
        if (transactionIsActive()) {
            getCurrentSession().commit();
            getCurrentSession().setAutoCommit(true);
        }

        stopSession();
    }

    public void rollback() {

        try {

            if (transactionIsActive()) {
                getCurrentSession().rollback();
                getCurrentSession().setAutoCommit(true);
            }

            stopSession();

        } catch (SQLException e) {
            System.out.println("Database error: " + e.getMessage());
        }
    }

    public PreparedStatement getPreparedStatement(String query) throws SQLException {
        return getCurrentSession().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
    }


    private void startSession() {
        if (connection == null) {
            connection = c3p0DataSource.getConnection();
        }
    }

    private void stopSession() {

        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                System.out.println("Error closing connection: " + e.getMessage());
            }
        }

        connection = null;

    }


    private Connection getCurrentSession() {
        startSession();
        return connection;
    }

    private boolean transactionIsActive() throws SQLException {
        return !getCurrentSession().getAutoCommit();
    }


}
