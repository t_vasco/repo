package org.academiadecodigo.streetcred.view;

import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.streetcred.controller.NewAccountController;
import org.academiadecodigo.streetcred.model.account.AccountType;

/**
 * A view shown at new account creation
 *
 * @see NewAccountController
 */
public class NewAccountView extends AbstractView {

    private NewAccountController newAccountController;

    /**
     * Sets the controller responsible for rendering the view
     *
     * @param newAccountController the controller to set
     */
    public void setNewAccountController(NewAccountController newAccountController) {
        this.newAccountController = newAccountController;
    }

    /**
     * @see View#show()
     */
    @Override
    public void show() {
        int accountId = newAccountController.createAccount(scanType());
        System.out.println("\n" + Messages.VIEW_NEW_ACCOUNT_MESSAGE + accountId);
    }

    private AccountType scanType() {
        MenuInputScanner scanner = new MenuInputScanner(AccountType.getTypes());
        scanner.setMessage("Choose account type");
        scanner.setError("Not a valid option");

        int userInput = prompt.getUserInput(scanner);
        return AccountType.values()[userInput - 1];

    }
}
