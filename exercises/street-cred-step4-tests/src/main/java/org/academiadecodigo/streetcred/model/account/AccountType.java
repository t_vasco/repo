package org.academiadecodigo.streetcred.model.account;

/**
 * The possible {@link Account} types
 */
public enum AccountType {

    /**
     * @see CheckingAccount
     */
    CHECKING,

    /**
     * @see SavingsAccount
     */
    SAVINGS;

    public static String[] getTypes() {
        String[] types = new String[values().length];
        for (int i = 0; i < values().length; i++) {
            types[i] = values()[i].name();
        }

        return types;
    }
}
