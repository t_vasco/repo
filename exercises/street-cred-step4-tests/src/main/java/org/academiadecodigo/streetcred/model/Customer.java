package org.academiadecodigo.streetcred.model;

import org.academiadecodigo.streetcred.model.account.Account;

import java.util.ArrayList;
import java.util.List;

/**
 * The customer model entity
 */
public class Customer extends AbstractModel {

    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private List<Account> accounts = new ArrayList<>();

    /**
     * Gets the firstName of the customer
     *
     * @return the customer firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the firstName of the customer
     *
     * @param firstName the first name to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the last name of the customer
     *
     * @return the customer last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the last name of the customer
     *
     * @param lastName the last name to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the email of the customer
     * @return the customer's email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email of the customer
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }


    /**
     * Gets the phone number of the customer
     * @return the customer phone number
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the phone number of the customer
     * @param phone the phone number to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Gets the customer accounts
     *
     * @return the accounts
     */
    public List<Account> getAccounts() {
        return accounts;
    }

    /**
     * Adds a new account to the customer
     *
     * @param account the account to add
     */
    public void addAccount(Account account) {

        if(account == null) {
            return;
        }

        accounts.add(account);
    }

    /**
     * Removes an account from the customer
     *
     * @param account the account to remove
     */
    public void removeAccount(Account account) {
        accounts.remove(account);
    }
}


