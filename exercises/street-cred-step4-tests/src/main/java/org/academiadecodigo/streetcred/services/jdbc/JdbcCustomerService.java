package org.academiadecodigo.streetcred.services.jdbc;

import org.academiadecodigo.streetcred.model.Customer;
import org.academiadecodigo.streetcred.model.account.Account;
import org.academiadecodigo.streetcred.persistence.C3p0DataSource;
import org.academiadecodigo.streetcred.persistence.SessionManager;
import org.academiadecodigo.streetcred.services.CustomerService;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class JdbcCustomerService implements CustomerService {

    JdbcAccountService jdbcAccountService;
    SessionManager sessionManager = null;

    public JdbcCustomerService(JdbcAccountService jdbcAccountService, SessionManager sessionManager){
        this.jdbcAccountService = jdbcAccountService;
        this.sessionManager = sessionManager;
    }


    @Override
    public Customer get(Integer id) {

        Customer customer = null;

        sessionManager.beginRead();

        String query = "SELECT * " +
                "FROM customer " +
                "WHERE id =?;";

        try{
            PreparedStatement statement = sessionManager.getPreparedStatement(query);
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()){
                String email = resultSet.getString("email");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String phone = resultSet.getString("phone");

                customer = new Customer();

                ResultSet resultSet1 = getAccountId(id);


                customer.setFirstName(firstName);
                customer.setLastName(lastName);
                customer.setEmail(email);
                customer.setPhone(phone);

                while (resultSet1.next()){

                    customer.addAccount(jdbcAccountService.get(resultSet1.getInt("id")));

                }
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customer;
    }


    public ResultSet getAccountId(Integer id){
        sessionManager.beginRead();
        ResultSet resultSet = null;

        String query = "SELECT * " +
                "FROM account " +
                "WHERE customer_id =?";
        try{
            PreparedStatement statement = sessionManager.getPreparedStatement(query);
            statement.setInt(1, id);

             resultSet = statement.executeQuery();


            return resultSet;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }

    @Override
    public List<Customer> list() {

        ArrayList<Customer> customerArrayList = null;

        sessionManager.beginRead();

        String query = "SELECT id FROM customer;";

        try{
            PreparedStatement statement = sessionManager.getPreparedStatement(query);

            ResultSet resultSet = statement.executeQuery();
            customerArrayList = new ArrayList<>();

            while (resultSet.next()){
                Customer customer = get(resultSet.getInt("id"));
                System.out.println(customer.getFirstName() + " " + customer.getAccounts());

                customerArrayList.add(customer);
            }
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return customerArrayList;
    }

    @Override
    public Set<Integer> listCustomerAccountIds(Integer id) {
        Set<Integer> accountIds = new HashSet<>();

        getAccountId(id);

       try{
           ResultSet resultSet = getAccountId(id);

           while (resultSet.next()){
               accountIds.add(resultSet.getInt(2));

           }
       }catch (SQLException ex){
           ex.printStackTrace();
       }

        return accountIds;
    }

    @Override
    public Double getBalance(Integer id) {

        double balance = 0.0;

        sessionManager.beginRead();

        String query = "SELECT SUM(balance) AS balance FROM account WHERE customer_id = ?";

        try{
            PreparedStatement statement = sessionManager.getPreparedStatement(query);
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()){

                balance = resultSet.getDouble("balance");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return balance;
    }

    @Override
    public void add(Customer customer) {

        PreparedStatement statement = null;
        sessionManager.beginWrite();

        try{
            String query = "INSERT INTO customer (email, first_name, last_name, phone)" +
                    "VALUES(?, ?, ?, ?);";

            statement = sessionManager.getPreparedStatement(query);

            statement.setString(1, customer.getEmail());
            statement.setString(2,customer.getFirstName());
            statement.setString(3, customer.getLastName());
            statement.setString(4,customer.getPhone());

            statement.executeUpdate();

        } catch (SQLException e) {
            sessionManager.rollback();
            e.printStackTrace();
        }
        finally {
            try {
                sessionManager.commit();
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

    }


    public static void main(String[] args) {
        C3p0DataSource c3p0DataSource = new C3p0DataSource();
        c3p0DataSource.start();
        SessionManager sessionManager = new SessionManager(c3p0DataSource);

        JdbcAccountService jdbcAccountService = new JdbcAccountService(sessionManager);
        JdbcCustomerService jdbcCustomerService = new JdbcCustomerService(jdbcAccountService, sessionManager);

        System.out.println(jdbcCustomerService.get(1).getFirstName()
                +" " +jdbcCustomerService.get(1).getLastName() + " Balance: "
        + jdbcCustomerService.getBalance(1));

    }
}
