package org.academiadecodigo.streetcred.services.jdbc;

import org.academiadecodigo.streetcred.model.account.Account;
import org.academiadecodigo.streetcred.model.account.AccountType;
import org.academiadecodigo.streetcred.model.account.CheckingAccount;
import org.academiadecodigo.streetcred.model.account.SavingsAccount;
import org.academiadecodigo.streetcred.persistence.C3p0DataSource;
import org.academiadecodigo.streetcred.persistence.SessionManager;
import org.academiadecodigo.streetcred.services.AccountService;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcAccountService implements AccountService {

    SessionManager sessionManager = null;

    public JdbcAccountService(SessionManager sessionManager){
        this.sessionManager =  sessionManager;
    }

    @Override
    public void add(Account account, Integer customerId) {

        PreparedStatement statement = null;
        sessionManager.beginWrite();

        try {
            String query = "INSERT INTO account(account_type, balance, customer_id) " +
                    " VALUES (?, ?, ?);";

            statement = sessionManager.getPreparedStatement(query);

            statement.setString(1, account.getAccountType() == AccountType.CHECKING ? "CHECKING" : "SAVINGS");
            statement.setInt(2, 100);
            statement.setInt(3, customerId);

            statement.executeUpdate();

        } catch (SQLException e) {
            sessionManager.rollback();
            e.printStackTrace();
        }
        finally {
            try {
                sessionManager.commit();

                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public Account get(Integer id) {

        Account account = null;

        sessionManager.beginRead();

        String query = "SELECT * " +
                "FROM account " +
                "WHERE id =?;";

        try {
            PreparedStatement statement = sessionManager.getPreparedStatement(query);
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()){
                String accountType = resultSet.getString("account_type");
                double balance = resultSet.getDouble("balance");

                if(accountType.equals("CHECKING")){
                    account = new CheckingAccount();
                }else{
                    account = new SavingsAccount();
                }

                account.setId(id);
                account.credit(balance);

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }


        return account;
    }

    @Override
    public void deposit(Integer id, double amount) {

    }

    @Override
    public void withdraw(Integer id, double amount) {

    }

    @Override
    public void transfer(Integer srcId, Integer dstId, double amount) {

    }

    public static void main(String[] args) {
        C3p0DataSource c3p0DataSource = new C3p0DataSource();
        c3p0DataSource.start();
        SessionManager sessionManager = new SessionManager(c3p0DataSource);

        JdbcAccountService jdbcAccountService = new JdbcAccountService(sessionManager);

        Account ac = new CheckingAccount();

        jdbcAccountService.get(1);
        jdbcAccountService.add(ac,1);
        System.out.println("Here");

    }
}


