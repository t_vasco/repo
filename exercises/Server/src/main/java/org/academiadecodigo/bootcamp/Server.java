package org.academiadecodigo.bootcamp;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;

public class Server {

    public static final String ABS_PATH = "/Users/codeforallsintra/Documents/t_vasco_repo/personal/exercises/Server/src/main/resources";

    private int port = 8080;
    private Socket clientSocket;
    private ServerSocket serverSocket;
    private File file;


    public static void main(String[] args) {
        Server server1 = new Server();
        server1.socket();
    }

    private void socket() {
        try {
            serverSocket = new ServerSocket(port);

            while (true) {
                clientSocket = serverSocket.accept();
                System.out.println("Connection established with: " + clientSocket.getInetAddress());
                readyToReceive();
            }

        } catch (IOException ex) {
            System.err.println("Not Able to establish connection: " + ex);

        } finally {
            close();
        }
    }

    private void readyToReceive() throws IOException {

            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            String input = in.readLine();
            System.out.println(input);
            if(!(input.substring(0,input.indexOf(" ")).equals("GET"))){
                notAllowed();
                return;
            }
            if((input.substring(0,input.indexOf(" ")).equals("GET"))){
            String verb = input.substring(0, input.indexOf(' '));
            String resource = input.substring(input.indexOf(' ') + 1, input.lastIndexOf(' '));
            String protocol = input.substring(input.lastIndexOf(' ') + 1);

            System.out.println("Verb: " + verb);
            System.out.println("Resource: " + resource);
            System.out.println("Protocol: " + protocol);

            checkVerb(verb, resource);
            }

    }



    private void checkVerb(String verb ,String resource) throws IOException{

        if (verb.startsWith("GET")) {
            if(resource.endsWith(".html")){
                sendHtml(resource);
                return;
            }
            if(resource.endsWith(".jpeg")){
                sendImage(resource);
                return;
            }
            sendUsererror("not_found.png");
        }

    }

    private void sendHtml(String resource) throws IOException{

        file = new File(ABS_PATH + resource);
        DataOutputStream dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());

        dataOutputStream.writeBytes("HTTP/1.0 200 OK\r\n" +
                "Content-Type: text/html; charset=UTF-8\r\n" +
                "Content-Length:" + file.length() +" \r\n\r\n");

        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        byte[] b1 = Files.readAllBytes(file.toPath());

        dataOutputStream.write(b1);
        dataOutputStream.flush();

        System.out.println("OK Sent");

    }

    private void sendImage(String resource)throws IOException{

        file = new File(ABS_PATH + resource);
        DataOutputStream dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());

        dataOutputStream.writeBytes("HTTP/1.0 200 OK\r\n" +
                "Content-Type: image.jpeg \r\n" +
                "Content-Length: " + file.length() + "\r\n" +
                "\r\n");


        byte[] i1 = Files.readAllBytes(file.toPath());
        dataOutputStream.write(i1);
        dataOutputStream.flush();
        System.out.println("OK Sent");
    }

    private void notAllowed() throws IOException{
        file = new File(ABS_PATH + "not_found.png");
        DataOutputStream dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());

        dataOutputStream.writeBytes("HTTP/1.0 405 Method Not Allowed\r\n" +
                "Content-Type: 405error.webp \r\n" +
                "Content-Length: " + file.length()+ "\r\n" +
                "\r\n");
        byte[] i1 = Files.readAllBytes(file.toPath());
        dataOutputStream.write(i1);
        dataOutputStream.flush();
        System.out.println("Method Not Allowed Sent.");

    }

    private void sendUsererror(String resource) throws IOException{
        file = new File(ABS_PATH + resource);

        DataOutputStream dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());

        dataOutputStream.writeBytes("HTTP/1.0 404 Not Found\r\n"
                                    + "Content-Type: not_found.png \r\n"
                                    + "Content-Length: " + file.length() + " \r\n\r\n");

        byte[] i1 = Files.readAllBytes(file.toPath());
        dataOutputStream.write(i1);
        dataOutputStream.flush();
        System.out.println("404 Error Sent.");
    }
    private void close() {
        try {
            serverSocket.close();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
