

import java.io.IOException;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client {

    public static void main(String[] args) {
        String message = "Teste message from Client";
        String hostName = "127.0.0.1";
        int portNumber = 5555;

        Logger log = Logger.getLogger("My logger");

        byte[] sendBuffer = new byte[1024];
        byte[] receiveBuffer = new byte[1024];


        try{
            DatagramSocket datagramSocket = new DatagramSocket();
            try{

                DatagramPacket sendPacket = new DatagramPacket(
                        sendBuffer, sendBuffer.length, InetAddress.getByName(hostName), portNumber);
                sendPacket.setData(message.getBytes());
                datagramSocket.send(sendPacket);


                DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
                datagramSocket.receive(receivePacket);

                message = new String(receivePacket.getData(), 0, receivePacket.getLength(), StandardCharsets.UTF_8);

                System.out.println(message);

            }
            finally {
                datagramSocket.close();
            }
        }


        catch (SocketException e){
            log.log(Level.SEVERE, "Method", e);
        }

        catch (IOException e){
            log.log(Level.SEVERE, "Method", e);
        }




    }



}
