import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;

public class Server {


    public static void main(String[] args) {

        String message;
        int portNum = 5555;
        byte[] sendBuffer = new byte[1024];
        byte[] receiveBuffer = new byte[1024];

        try{
            DatagramSocket datagramSocket = new DatagramSocket(portNum);
            try {
                DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
                datagramSocket.receive(receivePacket);

                message = new String (receivePacket.getData(), 0, receivePacket.getLength(), StandardCharsets.UTF_8).toUpperCase();


                DatagramPacket sendPacket = new DatagramPacket(sendBuffer,
                        sendBuffer.length, receivePacket.getAddress(), receivePacket.getPort());

                sendPacket.setData(message.getBytes());

                datagramSocket.send(sendPacket);
            }
            finally {
                datagramSocket.close();
            }
        }
        catch (SocketException e) {
            System.out.println(e);

        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
