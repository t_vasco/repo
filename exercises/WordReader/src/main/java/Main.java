import java.util.Iterator;

public class Main {
    private static final String FILE_PATH = "/Users/codeforallsintra/Documents/t_vasco_repo/personal/exercises/WordReader/src/main/resources/sample.txt";


    public static void main(String[] args) {

        WordReader wordReader = new WordReader(FILE_PATH);


 /*   wordReader.defineCurrentLine();
        wordReader.defineWord();
        wordReader.iterator().next();
        wordReader.defineCurrentLine();
        wordReader.defineWord();
        wordReader.iterator().next();
        wordReader.defineCurrentLine();
        wordReader.defineWord();
        wordReader.iterator().next();*/


        for (String word : wordReader) {
            System.out.println(word);

        }

        Iterator<String> iterator = wordReader.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
