import java.io.*;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class WordReader implements Iterable<String> {


    private BufferedReader buffRead = null;
    private FileReader fileReader;


    public WordReader(String FILE_PATH) {
        try {
            this.fileReader = new FileReader(FILE_PATH);
            this.buffRead = new BufferedReader(fileReader);
        } catch (FileNotFoundException ex) {
            System.err.println("File not found " + ex.getMessage());
        }
    }


    public String readPrint() {
        String thisLine = null;
        try {
            thisLine = buffRead.readLine();
            if (thisLine == null) {
                buffRead.close();
                return null;
            }
        } catch (IOException ex) {
            System.err.println("Unable to read this file " + ex.getMessage());
        }
        return thisLine;
    }


    public Iterator<String> iterator() {

        return new WordIterator();

    }

    private class WordIterator implements Iterator<String> {

        private String[] words;
        private String currentLine;
        private int index = 0;

        public WordIterator() {
            defineCurrentLine();
            defineWord();
        }

        public void defineCurrentLine() {
            currentLine = readPrint();
        }


        public void defineWord() {
            if (currentLine == null) {
                return;
            }
            words = currentLine.split(" ");
        }


        public String next() {

            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            defineWord();
            String result = words[index];
            index++;

            if (index == words.length) {
                defineCurrentLine();
                //defineWord();
                index = 0;
            }

            return result;
        }

        public boolean hasNext() {

            if (currentLine != null) {
                return true;
            }
            return false;
        }

        public void remove() {
        }
    }
}
