package org.academiadecodigo.bootcamp.mirc;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    private String host;
    private int port;

    private Socket clientSocket;
    private Scanner scanner;
    private boolean connected;
    private PrintWriter out;
    private BufferedReader in;



    private Client(){
        getUserInput();
        this.scanner = new Scanner (System.in);
    }


    public static void main(String[] args) {

        Client c1 = new Client();
        System.out.println("\n"+ "Client Created");

        try{
           c1.createSocket();
           c1.inStream();
           c1.stopConnection();
        }

        catch (IOException ex){
            System.err.println("PSVM Exception" + ex);
        }
    }

    private void createSocket() throws IOException{
        connected = true;
        try {
            clientSocket = new Socket(host, port);
            System.out.println("Establishing connection to: " + clientSocket.getRemoteSocketAddress());
        }catch (IOException ex){
            System.out.println("Create Socket Exeption " + ex);
        }
    }


    private void inStream() {
        String input;

        while(connected){
            try {
                input = scanner.nextLine();
                out = new PrintWriter(this.clientSocket.getOutputStream(), true);
                out.println(input);
                out.flush();

                if(input.equals("/quit")){
                    stopConnection();
                    System.out.println("You exited the chat");
                    break;
                }
                outStream();


            }catch (IOException ex){
                System.out.println("Input Stream Exception: " + ex);
            }

        }
        stopConnection();
    }


    private void outStream(){
        if(!connected){
            stopConnection();
        }
        String received;
        try {
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            while ((received = in.readLine()) != null) {
                System.out.println(received);
                inStream();
            }

        } catch (IOException ex) {
            System.err.println("Out Stream Exception: " + ex);
        }
    }


    private void stopConnection(){
        try{
            in.close();
            out.close();
            clientSocket.close();
        } catch (IOException ex){
            System.out.println("Close Exception: " + ex);
        }
    }


    private void getUserInput(){
        try{
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Hostname? ");
            host = in.readLine();

            System.out.println("Port? ");
            port = Integer.parseInt(in.readLine());
        }catch (IOException ex){
            System.err.println("User Input Error " + ex);
        }
    }
}
