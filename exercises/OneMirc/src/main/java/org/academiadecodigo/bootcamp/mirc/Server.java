package org.academiadecodigo.bootcamp.mirc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {

    private Socket clientSocket;
    private boolean connected;
    private BufferedReader in;
    private PrintWriter out;
    private String nickName;
    private String dataReceived;
    private ServerSocket serverSocket;

    private Server(){
        int userCount = 1;
        nickName = "user".concat(Integer.toString(userCount));
        userCount++;
    }


    public static void main(String[] args) {
        Server s1 = new Server();
        System.out.println("Server Created " + "\n");
        try{
            s1.createSocket(4444);
            System.out.println("S1 Server started ");


        }catch (IOException ex){
            System.err.println("PSVM exception " + ex);
        }
    }


    private void createSocket(int port) throws IOException{
        try{
            serverSocket = new ServerSocket(port);
            System.out.println("Server socket created " + port);

            connected = true;
        clientSocket = serverSocket.accept();
        System.out.println("Client Socket Created");
        readyToReceive();
        }finally {

            close();
        }

    }


    private void readyToReceive() throws IOException{
        while(connected) {
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            while ((dataReceived = in.readLine()) != null) {
                checkKeyword();
                System.out.println(nickName + ": " + dataReceived);

                if (dataReceived.equals("/quit")) {
                    connected = false;
                    in.close();
                    return;
                }
                inputStream();
            }
        }
    }


    private void inputStream() throws IOException{

        String dataSend;

        while(connected) {
            //dataSend = scanner.nextLine();
            dataSend = nickName +": "+ dataReceived;
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            out.println(dataSend);
            out.flush();
            readyToReceive();
        }
        close();
    }


    private void close() throws IOException{
        serverSocket.close();
        in.close();
        out.close();
        clientSocket.close();
    }


    private void checkKeyword(){
        CharSequence cs1 = "/nick";
        CharSequence cs2 = "/shout";
        CharSequence cs3 = "/shrug";

        if(dataReceived.contains(cs1)){
            nickName = dataReceived.substring(cs1.length()).trim();
        }

        if (dataReceived.contains(cs2)) {
            System.out.println("check UpperCase");
            String message = dataReceived.substring(cs2.length()).trim();
            dataReceived = message.toUpperCase();
        }

        if(dataReceived.contains(cs3)){
            String meh = "\\_(\")_/";
            dataReceived = dataReceived.replaceAll(cs3.toString(), meh);
        }
    }
}
