package AddInteger;

import java.util.Scanner;

public class AddTInteger {

    private Scanner reader = new Scanner(System.in);


    public static void main(String[] args) {
        AddTInteger a1 = new AddTInteger();
        int num1 = a1.getNum();
        int num2 = a1.getNum();
        a1.sum(num2, num2);
    }

    private int getNum(){
        System.out.println("Enter a number.");
        int input = reader.nextInt();

        reader.close();
        return input;
    }

    private void sum(int num1, int num2){
        int total = num1 + num2;
        System.out.println("The total is: " + total);
    }


}
