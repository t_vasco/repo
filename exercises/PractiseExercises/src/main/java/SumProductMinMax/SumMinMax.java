package SumProductMinMax;

import java.util.InputMismatchException;
import java.util.Scanner;

public class SumMinMax {

    private int[] num = new int[5];
    private Scanner reader = new Scanner(System.in);
    private int sum;
    private int product = 1;
    private int min;
    private int max;


    public static void main(String[] args) {
        SumMinMax sum = new SumMinMax();
        sum.receiveInput();
        sum.cacl();
        sum.minMax();
    }

    //TODO IF USER DOES NOT PUT A NUMER IT ASKS AGAIN
    private void receiveInput(){
        for(int i = 0; i <num.length; i++) {
            System.out.println("Please enter a number: ");
            try {
                num[i] = reader.nextInt();
                System.out.println("Num: " + num[i]);

            } catch (InputMismatchException ex) {
                System.out.println("I don't think that's a number");
            }
        }
    }
    private void cacl(){
        for(int i = 0; i< num.length; i++){
             sum += num[i];
             product *= num[i];
        }
        System.out.println("Sum: " + sum + ". Product: " + product + ".");
    }

    private void minMax(){
        int currentMin = num[0];
        int currentMax = num[0];
        for(int i = 0; i < num.length; i++){
            for(int j = 0; j< num.length; j++){

                if(num[i] > num[j]){
                    currentMin = num[j];
                }
                min = currentMin;
                ;
                if(num[i] < num[j]){
                    currentMax = num[j];
                }
                max = currentMax;
            }
        }
        System.out.println("Min: " + min + ". Max: " + max + ".");
    }
}
