package CircleComputation;

import java.util.InputMismatchException;
import java.util.Scanner;

public class CircleComputation {

    private Scanner reader = new Scanner(System.in);

    private double requestNum(){
        System.out.println("Please enter a double number.");
        try {
            return reader.nextDouble();
        }catch (InputMismatchException ex){
            System.out.println("That is not a valid number");
        }
        return 0.0;
    }

    public void calcDiam(){
        System.out.println(requestNum() * 2);
    }

    public void calcArea(){
        System.out.println(Math.PI * requestNum()*requestNum());
    }

    public void calcCircum(){
        System.out.println(Math.PI * requestNum() * 2);
    }
}
