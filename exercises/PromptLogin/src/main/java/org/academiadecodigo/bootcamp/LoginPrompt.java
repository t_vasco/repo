package org.academiadecodigo.bootcamp;

import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;

import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginPrompt {


    private UserStore u1;

    private Prompt prompt = new Prompt(System.in, System.out);

    private Logger logger = Logger.getLogger(LoginPrompt.class.getName());


    public LoginPrompt(UserStore userStore){
        this.u1 = userStore;
    }



    public void promptUser(){
        boolean loggedIn = false;
        while(loggedIn == false){

            StringInputScanner question1 = new StringInputScanner();
            question1.setMessage("What is your username?");

            String userName = prompt.getUserInput(question1);


            StringInputScanner question2 = new StringInputScanner();
            question2.setMessage("Insert your PIN.");

            String password = prompt.getUserInput(question2);

            if(confirmUser(userName)){
                if(compare(password, userName)){
                    System.out.println("You logged in.");
                    loggedIn = true;
                    return;
                }
                System.out.println("Login attempt failed, either your username, or your password are incorrect.");
            }


        }
    }




    private boolean confirmUser(String userName){


        if((u1.hmap.containsKey(userName))){

            logger.log(Level.FINE, "Username correct.");
            return true;
        }
        logger.log(Level.SEVERE, "Incorrect Username");
        return false;
    }

    private boolean compare(String password, String userName){
        if((password.equals(u1.hmap.get(userName)))){
            logger.log(Level.FINE, "Password Correct.");
            return true;
        }
        logger.log(Level.SEVERE, "Incorrect password for user: " + userName);
        return false;
    }

}
