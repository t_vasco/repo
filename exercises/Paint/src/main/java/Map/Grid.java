package Map;


import java.awt.*;
import java.io.BufferedReader;
import java.io.Reader;

public class Grid {

    public static final int CELL_SIZE = 15;
    public static final int PADDING = 10;


    private Cell[][] cellArray;
    public static int cols;
    public static int rows;


    public Grid(int cols, int rows) {
        this.cellArray = new Cell[cols][rows];
        this.cols = cols;
        this.rows = rows;
        positions();
    }



    public void positions() {

        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows; row++) {
                cellArray[col][row] = new Cell(col, row);
            }
        }
    }

    public void paintCell(int col, int row ){
        cellArray[col][row].setPainted();
    }
    public void clearPaint(int col, int row){
        cellArray[col][row].clearPaint();
    }


    public void saveCanvas(){

    }

}


