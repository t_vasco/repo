package Map;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;


public class Cursor extends Cell{


    public Cursor(){
        super(0,0);
        drawCursor();

    }

    private void drawCursor(){
        rectangle.draw();
        rectangle.fill();
        setColour(Color.PINK);
    }

    public void moveCursor(KeyboardEvent keyboardEvent){
        switch (keyboardEvent.getKey()){
            case KeyboardEvent.KEY_LEFT:
                moveCursorLeft();

                break;
            case KeyboardEvent.KEY_RIGHT:
                moveCursorRight();
                break;
            case KeyboardEvent.KEY_DOWN:
                moveCursorDown();
                break;
            case KeyboardEvent.KEY_UP:
               moveCursorUp();
               break;
        }
    }

    private void moveCursorLeft(){
        if(rectangle.getX() == Grid.PADDING){
            return;
        }
        rectangle.translate(-Grid.CELL_SIZE, 0);
        this.setCol(getCol()-1);

    }


    private void moveCursorRight(){
        if(rectangle.getX() >= (Grid.CELL_SIZE * Grid.cols) - Grid.CELL_SIZE ){
            return;
        }
        rectangle.translate(Grid.CELL_SIZE, 0);
        this.setCol(getCol()+1);

    }


    private void moveCursorUp(){

        if(rectangle.getY() == Grid.PADDING){
            return;
        }
        rectangle.translate(0,  - Grid.CELL_SIZE);
        this.setRow(getRow()-1);

    }


    private void moveCursorDown(){
        if(rectangle.getY() >= (Grid.CELL_SIZE*Grid.rows) - Grid.CELL_SIZE){
            return;
        }
        rectangle.translate(0, Grid.CELL_SIZE);
        this.setRow(getRow()+1);

    }
}
