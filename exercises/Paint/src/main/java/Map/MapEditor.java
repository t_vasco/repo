package Map;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class MapEditor  implements KeyboardHandler {

    private Keyboard keyboard;
    private Cursor cursor;
    private Grid grid;

    public MapEditor(int cols, int rows){
        keyboard = new Keyboard (this);
        cursor = new Cursor();
        grid = new Grid(cols, rows);

    }

    public void init(){
        KeyboardEvent left = new KeyboardEvent();
        left.setKey(KeyboardEvent.KEY_LEFT);
        left.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent right = new KeyboardEvent();
        right.setKey(KeyboardEvent.KEY_RIGHT);
        right.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent up = new KeyboardEvent();
        up.setKey(KeyboardEvent.KEY_UP);
        up.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent down = new KeyboardEvent();
        down.setKey(KeyboardEvent.KEY_DOWN);
        down.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent paint = new KeyboardEvent();
        paint.setKey(KeyboardEvent.KEY_SPACE);
        paint.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent clearPaint = new KeyboardEvent();
        clearPaint.setKey(KeyboardEvent.KEY_C);
        clearPaint.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent saveCanvas = new KeyboardEvent();
        saveCanvas.setKey(KeyboardEvent.KEY_S);
        saveCanvas.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);


        keyboard.addEventListener(left);
        keyboard.addEventListener(right);
        keyboard.addEventListener(up);
        keyboard.addEventListener(down);
        keyboard.addEventListener(paint);
        keyboard.addEventListener(clearPaint);
        keyboard.addEventListener(saveCanvas);
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_LEFT:
                cursor.moveCursor(keyboardEvent);
                break;
            case KeyboardEvent.KEY_RIGHT:
                cursor.moveCursor(keyboardEvent);
                break;
            case KeyboardEvent.KEY_UP:
                cursor.moveCursor(keyboardEvent);
                break;
            case KeyboardEvent.KEY_DOWN:
                cursor.moveCursor(keyboardEvent);
                break;
            case KeyboardEvent.KEY_SPACE:
                grid.paintCell(cursor.getCol(), cursor.getRow());
                break;
            case KeyboardEvent.KEY_C:
                grid.clearPaint(cursor.getCol(), cursor.getRow());
                break;

            case KeyboardEvent.KEY_S:
                grid.saveCanvas();
                break;

        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }

    public Cursor getCursor() {
        return cursor;
    }
}
