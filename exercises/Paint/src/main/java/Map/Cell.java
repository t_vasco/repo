package Map;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Cell {

    private int col;
    private int row;
    protected Rectangle rectangle;


    public Cell(int col, int row) {
        this.rectangle = new Rectangle(col * Grid.CELL_SIZE+Grid.PADDING, row * Grid.CELL_SIZE + Grid.PADDING, Grid.CELL_SIZE,Grid.CELL_SIZE);
        this.col = col;
        this.row = row;
        draw();
    }

    public void draw() {
        rectangle.draw();
    }

    public void setColour(Color colour){
        rectangle.setColor(colour);
    }



    public void setPainted() {
        if(rectangle.isFilled()){
            rectangle.setColor(Color.BLACK);
            rectangle.draw();
        }
        rectangle.setColor(Color.GREEN);
        rectangle.fill();
    }

    public void clearPaint(){
        rectangle.setColor(Color.BLACK);
        draw();
    }



    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public void setRow(int row) {
        this.row = row;
    }
}
