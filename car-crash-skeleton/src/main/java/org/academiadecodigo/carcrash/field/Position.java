package org.academiadecodigo.carcrash.field;

public class Position {
    private int col;
    private int row;

    public Position(int col, int row) {
        this.col = col;
        this.row = row;
    }

    public Position() {
        col = generateRandomPos(Field.getWidth());
        row = generateRandomPos(Field.getHeight());
    }

    public int generateRandomPos(int max) {
        return (int) (Math.random() * max);
    }


    public void moveLeft() {
        if(col == 0){
            col ++;
            return;
        }
        col--;
    }

    public void moveRight() {
        if(col == Field.getWidth()-1){
            col--;
            return;
        }
        col++;
    }

    public void moveDown(){
        if(row == Field.getHeight()-1){
            col--;
            return;
        }
        row++;
    }

    public void moveUp() {
        if(row >= 0){
            row--;}
        if(row < 0) {
            row++;
        }
    }


    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public void moveToDirection(Direction currentDirection) {



        switch (currentDirection) {
            case UP:
                moveUp();
                break;
            case RIGHT:
                moveRight();
                break;
            case LEFT:
                moveLeft();
                break;
            case DOWN:
                moveDown();
                break;


        }
    }

}
