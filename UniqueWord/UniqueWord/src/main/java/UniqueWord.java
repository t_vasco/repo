import javax.swing.text.html.HTMLDocument;
import java.util.HashSet;
import java.util.Set;

public class UniqueWord implements Iterable<String> {

    private <String> set = new HashSet<>();

    private Set<String> words = new HashSet<~>();

    public UniqueWord(String string){
        for(String word : string.split(" ")){
            words.add(word);
        }
    }


    @Override
    public Iterator<String> iterator(){
        return words.iterator();
    }

}



