import java.util.PriorityQueue;

public class TodoList {

    private PriorityQueue <TodoItem> queu = new PriorityQueue<>();


    public TodoList(){
        queu = new PriorityQueue<TodoItem>(){
        };


    }
    public void add (Importance importance, int priority, String item ){
        queu.add(new TodoItem(importance, priority, item));
    }

    public TodoItem poll(){
        return queu.remove();
    }

    public boolean isEmpty(){
        return queu.isEmpty();
    }



    private class TodoItem implements Comparable<TodoItem>{
        private Importance importance;
        private int priority;
        private String item;

        public TodoItem(Importance importance, int priority, String item){
            this.importance = importance;
            this. priority = priority;
            this.item = item;
        }

        @Override
        public int compareTo(TodoItem todoItem){

            System.out.println(importance + " " + todoItem.importance);

            if(importance.compareTo(todoItem.importance) == 0){
                return priority - todoItem.priority;
            }

            return importance.compareTo(todoItem.importance);

        }


        @Override
        public String toString(){
            return "TodoItem{" +
                    "importance =" + importance +
                    "priority=" + priority +
                    ", item='" + item + '\'' +
                    "}";
        }

        public Importance getImportance() {
            return importance;
        }

        public int getPriority() {
            return priority;
        }

        public String getItem() {
            return item;
        }

    }


    public boolean remove(){
        return queu.remove(false);
    }
}
