import java.util.Comparator;

public class TodoItem implements Comparable <TodoItem> {


    private Importance importance;
    private int priority;
    private String item;

    public TodoItem(Importance importance, int priority, String item){
        this.importance = importance;
        this. priority = priority;
        this.item = item;
    }

@Override
    public int compareTo(TodoItem todoItem){


        return(this.priority - getPriority());

}

    public Importance getImportance() {
        return importance;
    }

    public int getPriority() {
        return priority;
    }

    public String getItem() {
        return item;
    }
}
