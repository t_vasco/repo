public class Main {

    public static void main(String[] args) {


        TodoList todoList = new TodoList();

        todoList.add(Importance.HIGH, 1, "Study Java");
        todoList.add(Importance.LOW, 1, "Play Playstation");
        todoList.add(Importance.MEDIUM, 1, "Eat");
        todoList.add(Importance.MEDIUM, 2, "Shower");
        todoList.add(Importance.HIGH, 2, "Finish the course");
        todoList.add(Importance.LOW, 2, "Watch TV");
        todoList.add(Importance.HIGH, 3, "Study Java2");
        todoList.add(Importance.MEDIUM, 3, "Study Java3");
        todoList.add(Importance.LOW, 3, "Study Java 4");


        while(!todoList.isEmpty()){
            System.out.println(todoList.poll());
        }


    }


}
