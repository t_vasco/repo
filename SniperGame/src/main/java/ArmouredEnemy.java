public class ArmouredEnemy extends Enemy{


    //Armoured Enemy Properties
    private int armour;

    public ArmouredEnemy(int armour){
        super();
        this.armour = armour;
    }

    //Armoured Enemy Methods

    public void hit(int shoot){

    }

    public int getArmour() {
        return armour;
    }

    public void setArmour(int armour) {
        this.armour = armour;
    }
}
/*
*
* Extends from Enemy * an Armour
* Override no Hit para
*   primeiro reduzir a Armour
*   Quando a armour for menor que cada hit, guardar a diferença e depois invocar o método normal para tirar a diferença na health;
* */