public abstract class Enemy extends GameObject {

    //Enemy Properties

    private int health = 50;
    private boolean isDead;


    public Enemy() {
        isDead = false;
    }
    //Enemy Methods

    public boolean isDead() {


        return isDead;
    }

    public void hit(int shoot) {
        health -= shoot;
        if (health <= 0) {
            isDead = true;
            System.out.println("I am Dead");
        }
    }

    public String getMessage() {

        System.out.println("Hello");
        return null;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }
}



/*
* props health dead
*
* Constructor With Life
* Getter & Setters
*
* Met
*
* Hit
*   verificar que a pessoa não fica com vida negativa.
*   Possível emcapsular o Die em um método diferente.
*   Alterar o estado do Enemy;
*
*
* */
