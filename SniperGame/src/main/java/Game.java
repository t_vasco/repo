public class Game {


    //Game Properties
    private GameObject[] gameObjects;
    private SniperRifle sniperRifle;
    private int shotsFired;


    //Game Methods

    public Game(int numObjects){
        this.gameObjects = createObjects(numObjects);
    }

    public void start() {
        for(int i = 0; i <gameObjects.length; i++){
            SniperRifle.shoot();
        }
    }


    public GameObject[] createObjects(int num) {

        GameObject[] gameObjects = new GameObject[num];


        //int probability = 8;
        //if (Math.random() > ((double) probability / 10)) {

            for (int i = 0; i < num; i++) {
                GameObject object;
                object = new ArmouredEnemy(10);
                gameObjects[i] = object;
            }

            return gameObjects;
        //}

    }

}



/*
* Class Game
* Constructor e atribuir valores adequados
*
*
* GameObject - Devolve uma Array de GO
*
* Declarar logo o Array de Game Objects
*
* Declarar um Ternário, para criar uma Tree ou invocar um Método numa classe diferente (enemy factory)
* No Class Enemy Factory A probabilidade de criar cada tipo é 50% e atribuir o Health como parâmetro e o Armour como Parâmetro;
*
*
* Método Start -
* Sout da mensagem específica a cada Class type
*
*Enhanced Loop()
*   limitar os targets aos enemies com o safecast
*   While the target not dead, Sniper.Shoot(O target) + Print uma mensagem bonita;
*
*
*
*
* */