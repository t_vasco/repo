package org.academiadecodigo.bootcamp.tutorial;

public class Tutorial {

    // Variables

    // In Java, we regularly need to store our values somewhere for later use; this is where variables come in.
    // Variables are simply memory addresses that are reserved for a type of data under a certain name.
    // This means that we can access these memory addresses by the name we've given them during the variable declaration.
    // When accessing these variables, we can either read or assign value to it.


    //// Declaring a variable


    // In Java, to declare a variable, we need 2 things: The type of the variable and its name.
    // The variable type should precede its name in the declaration and it represents what kind of values
    // we'll be allowed to store "inside" the variable, AKA in those memory addresses.

    // <type> <name> -> int age; || String name; || boolean dead; || Person employee; etc...

    // This is what is known as declaring a variable. Whenever we have a type, be it a primitive type or reference type
    // followed by a name, we know that a variable is being declared.

    // Besides what's written above, there are a couple more rules to declaring a variable.
    //// Its name should be in camelCase, and its name CANNOT start with a number (go ahead and try it!).

    // Please do note that every primitive type variable has a specific default value.
    // Every reference type variable has a default value of null.


    //// Assigning a value to a variable

    // Now, it's great that we know how to declare variables, and what they are, but this falls a bit short if we're
    // not actually using them for anything. We declare variables when we want to store a value for later use.
    // So how do I store a value in a variable? Java has an operator just for that -> =
    // The equals sign (=) is Java's assignment operator.
    // This means that the value written to its right, will be assigned (stored) in the variable written to its left side.


    // If we wanted to store the number 8, we could declare a variable of type int, give it
    // a name that described the value it'll hold and then assign it a value of the same type:
    // (Variable declaration) int age;
    // (Assigning it a value) age = 56;

    // This is all fine and dandy, but it seems a bit too verbose for such a simple operation.
    // There are situations that call for this, such as not knowing the value we want to assign yet.
    //// (Like in the CarFactory example from Car Crash!)
    // Though most of the time, we'll want to declare a variable and assign it a value on the same line.

    //     Person employee                  = new Person();
    //     String name                      = "John Doe";
    // |--- Variable declaration ---| |--- Value assignment ---|


    //// Types of variables


    // In Java there are actually 4(!!!) types of variables
    // Local variables
    // Method parameters
    // Instance or non-static properties/fields
    // Class or static properties/fields


    ////// Local variables

    // A local variable is a variable that is declared inside a methods body (inside the methods curly brackets -> {})

    public void printHeyo() {
        String message = "Heyo";
        System.out.println(message);
    }

    // message is an example of a local variable.
    // It was declared just like we've learned above! It was also immediately assigned a value.
    // It's called a local variable because we can only read or write its value inside the method's body.


    // In this particular case, the value assigned to message is from a reference type, more specifically, a String.
    // This means that message does not actually contain the sequence of characters Heyo.
    // It contains a reference to that object.
    // If you're not 110% sure what this means, read up on the Java Memory Model through our slides.
    // After you're done doing that, do it again, and then come back.


    //// Method parameters

    // A method parameter is a variable that will store the value of an argument when a method is invoked.

    public void printInt(int age) {
        System.out.println(age);
    }

    // Method parameters are useful because they allow whoever is invoking our method to give it information to work with.
    // As opposed to printHeyo, where it'll always print "Heyo", printInt is much cooler, and it will print
    // any int we pass it as an argument when we invoke it.

    // printInt(8) -> int age = 8 -> System.out.println(age) -> System.out.println(8) -> prints 8
    // printInt(10) -> int age = 10 -> System.out.println(age) -> System.out.println(10) -> prints 10

    // With a method parameter, similarly to a local variable, we can only read its value inside the methods body
    // However, there is a big difference.
    // We can write(assign) its value from outside the method's body!
    // When we invoke printInt, that's exactly what we're doing.
    // printInt(5) means we're assigning the value 5 to the method parameter int age.

    // Method parameters allow us to write methods that will work with values that are not known yet.
    // These values are only decided when the method is invoked! And that is preeeetty cool.
    // This way we can write a method that will work for ANY value of a given type, even if we don't know
    // what that value is beforehand.


    //// Instance or non-static properties/fields

    // Instance properties are variables which store values associated with a particular instance of a class.
    // They allow us to create two Person objects with them having different names!

    // Go take a look at the class Person in this package and then look at the following method:

    public void instanceVariableExample() {
        Person person1 = new Person("John Doe");
        Person person2 = new Person("Jane Doe");

        person1.sayName(); // prints John Doe
        person2.sayName(); // prints Jane Doe

        // private int a = 10; -> compilation error
    }

    // Here we can see that, despite both objects being of the type Person, each instance will have
    // their own variable String name. This is why non-static properties are called instance variables.
    // Non-static varaibles CANNOT be declared inside methods; be it their body or their parameter list.


    //// Class or static properties/fields

    // Class properties are variables which store values associated a particular class.
    // This means that the values stored in static fields are not associated with ANY particular instance.
    // It does not matter if I have 0 or 10 or 1467235467523 instances of Person, the value stored in a static field
    // is not stored in ANY of them. The value belongs to the class itself, not to the instances of the class.

    // Go take a look at the class Cookie and then come back.

    // But there is a problem with this...

    public void cookieTrouble() {
        Cookie raisins = new Cookie();
        Cookie chocolateChip = new Cookie();
        Cookie mint = new Cookie();
        Cookie peanutButter = new Cookie();

        System.out.println(Cookie.numberProduced); // prints 4

        Cookie.numberProduced = 0;

        System.out.println(Cookie.numberProduced); // prints 0
    }

    // Having a public static field means trouble!
    // Any object, anywhere in your application can change this value!
    // Static fields should be used with care, and are usually used as constants, meaning, its value is immutable.


    // Great, now that you're well read about variables, let's skip to Exercise.java and practice what we've learned.

}
