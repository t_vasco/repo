package ArabNight;

public class Genie {

    //properties from  Super class Genie
    private int maxWishes;
    private int concededWishes;

    //Constructor method from Super Class Genie

    public Genie (int maxWishes){
        this.maxWishes = maxWishes;
    }


    //Other methods from Super Class Genie

    //Check if Genie can grant Wish
    public boolean canGrantWish(){return (this.concededWishes <= this.maxWishes);


    }

    public void concedeWish(){if(!canGrantWish()){
        System.out.println("No More Wishes for you");
        return;
    }
        this.concededWishes++;
        System.out.println("I will grant your wish.");
    }

    //Getters and Setters from Super Class Genie

    public boolean canConcedeWish(){
        return getMaxWishes() - getConcededWishes() > 1;}



    public int getMaxWishes() {
        return maxWishes;
    }

    public int getConcededWishes(){
    return concededWishes;}

    public void setMaxWishes(int maxWishes){
        this.maxWishes = maxWishes;
    }

}

