package ArabNight;

public class ArabianNight {

    private static Genie[] genies;

    public static void main(String[] args){

        Lamp lamp1 = new Lamp(5);
        Lamp lamp2 = new Lamp(5);

        lamp1.compareLamp(lamp2);

        genies = new Genie[]{
                lamp1.rub(),
                lamp1.rub(),
                lamp1.rub(),
                lamp1.rub(),
                lamp1.rub(),
                lamp1.rub(),
                lamp1.rub(),
                lamp1.rub(),
                lamp1.rub()
        };

        lamp1.compareLamp(lamp2);

        genies[0].concedeWish();
        genies[0].concedeWish();
        genies[0].concedeWish();
        genies[0].concedeWish();
        genies[0].concedeWish();
        genies[0].concedeWish();
        genies[4].concedeWish();
        genies[4].concedeWish();
        genies[4].concedeWish();
        genies[4].concedeWish();
        genies[4].concedeWish();
        genies[4].concedeWish();

        lamp1.recharge(genies[5]);

    }

}
