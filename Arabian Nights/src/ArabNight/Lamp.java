package ArabNight;

public class Lamp {

    //Properties from the class Lamp.

    private int maxNumGenies;
    private int rechargeCount;
    private int geniesMade;
    private boolean recycledGenieOut;
    // private Genie[] genies;

// Constructors from class Lamp

    public Lamp(int maxNumGenies) {
        this.maxNumGenies = maxNumGenies;
    }

    //Other methods from class Lamp

    //Method to compare if 2 lamps are the same
    public void compareLamp(Lamp lamp) {
        if (this.maxNumGenies == lamp.getMaxNumGenies() &&
                this.rechargeCount == lamp.getRechargeCount() &&
                this.geniesMade == lamp.getGeniesMade()) {
            System.out.println("\nAmazing I have two similar lamps!");
            return;
        }
        System.out.println("\nThese lamps are not equal.");

    }

    //@ Method to recharge the lamp, when the maximum number of genies is reached.
    public void recharge(RecyDemon recyDemon) {
        if (recyDemon.isRecycled()) {
            return;
        }
        geniesMade = 0;
        rechargeCount++;
        recyDemon.Recycle();
        System.out.println("WHOOOO I feel Recharged");

    }


    //Method to calculate the remaining number of genies.
    public int remainingGenies() {
        return getMaxNumGenies() - getGeniesMade();
    }


    //Method to rub the lamp so that you get a genie
    public Genie rub() {
        Genie genie;

        if (recycledGenieOut) {
            System.out.println("Nop dude, No more genies for you you greedy bastard. Recharge me or no more genies");
            return null;
        }
        if (remainingGenies() <= 1) {
            genie = new RecyDemon(1);
            recycledGenieOut = true;
            System.out.println("I will grant you a Recyclable Demon.");
        }
        geniesMade++;

        if (geniesMade % 2 == 0) {
            genie = new FriendGenie(3);
            System.out.println("I am a Friendly Genie.");
        } else {
            genie = new GrumpGenie(1);
            System.out.println("I am your Grumpy Genie.");
        }
        System.out.println("\nI am your Genie, how can I serve you?\n");
        return genie;
    }


    //Method to generate genies


    //Getters and Setters for the lamp


    public int getMaxNumGenies() {
        return maxNumGenies;
    }

    public int getRechargeCount() {
        return rechargeCount;
    }

    public int getGeniesMade() {
        return geniesMade;
    }

    //public Genie getGenis(){ return genies[];}

}
