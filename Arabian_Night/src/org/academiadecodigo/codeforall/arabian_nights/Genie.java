package org.academiadecodigo.codeforall.arabian_nights;

public class Genie {

    //Genie Properties
    private int maxWishes;
    private int concededWishes = 0;


    //Genie Constructor
    public Genie (int maxWishes){
        this.maxWishes = maxWishes;
    }

    //Genie Methods

    public void concedeWish(){
        if(concededWishes == maxWishes){
            System.out.println("You have no mode wishes available to make.");
            return;
        }
        concededWishes ++;
        System.out.println("I shall grant your wish! Remember you have only " + (maxWishes-concededWishes) + " left to make.");
    }

    public int getMaxWishes(){
        return maxWishes;
    }
    public int getConcededWishes(){
        return concededWishes;
    }

    public void setConcededWishes(){
        this.concededWishes = concededWishes;
    }
    public void setMaxWishes(int increase){
        this.maxWishes = maxWishes;

    }


}
