package org.academiadecodigo.codeforall.arabian_nights;

public class Lamp {

    //Properties from class super Lamp

    private int maxNumGenies;
    private int rechargeCount = 0;
    private int geniesMade = 0;


    //Lamp Constructor

    Lamp(int maxNumGenies) {
        this.maxNumGenies = maxNumGenies;
    }

    // Lamp Methods

    public void compareLamp(Lamp lamp) {
        if (this.maxNumGenies == lamp.getMaxNumGenies() &&
                this.rechargeCount == lamp.getRechargeCount() &&
                this.geniesMade == lamp.getGeniesMade()) {
            System.out.println("Amazing I have 2 equal lamps.");
        }
    System.out.println("These 2 lamps are not the same!");

    }

    public void recharge(RecyDemon recyDemon) {
        if(recyDemon.isRecycled()){
            return;
        }
        rechargeCount++;
        recyDemon.recycle();

    }

    public void remainingGenies(int maxNumGenies, int geniesMade) {
        int remainGenies = maxNumGenies - geniesMade;

    }

    public Genie rub() {

        Genie genie;

        if (maxNumGenies < geniesMade) {
            genie = new RecyDemon(3);
            System.out.println("A Recyclable Demon for you Wishes");
        }
        geniesMade++;

        if(geniesMade % 2  == 0){
            genie = new FriendGenie(3);
            System.out.println("A Friendly Genie for your wishes");
        } else{
            genie = new GrumpGenie(3);
            System.out.println("A Grumpie Genie for your wishes");
        }
        return genie;
    }


    //Lamp Getters and Setters


    public int getGeniesMade() {
        return geniesMade;
    }

    public int getMaxNumGenies() {
        return maxNumGenies;
    }

    public int getRechargeCount() {
        return rechargeCount;
    }

}
