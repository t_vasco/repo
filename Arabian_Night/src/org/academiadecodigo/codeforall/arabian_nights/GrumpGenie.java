package org.academiadecodigo.codeforall.arabian_nights;

public class GrumpGenie extends Genie {

    public GrumpGenie (int maxWishes){
        super(maxWishes);
    }


    @Override
    public void concedeWish(){
        if(getConcededWishes() > 0){
            System.out.println("I don't feel like conceding you any more wishes.");
            return;
        }
        super.concedeWish();
    }


}
