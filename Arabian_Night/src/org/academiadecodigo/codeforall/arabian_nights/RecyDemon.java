package org.academiadecodigo.codeforall.arabian_nights;

public class RecyDemon extends Genie {

    private boolean recycled = false;
    private int wishIncrease = 1;

    public RecyDemon(int maxWishes){
        super(maxWishes);
    }

    @Override
    public void concedeWish(){
        super.concedeWish();
        setMaxWishes(wishIncrease++);

    }

    public void recycle(){
        recycled = true;
    }

    public boolean isRecycled(){
        return recycled;
    }

}
