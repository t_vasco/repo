package org.academiadecodigo.codeforall.MoneyInTheBank;

public class Atm {

    //Atm Properties

    private int balance;
    private boolean login;
    private int loginPin;
    private int loginAtempts;

    //Atm Constructors
    public Atm(int balance, int pin){
        this.balance = balance;
        this.loginPin = pin;

    }

    //Atm Methods

    public void withdrawMoney(int money, int userPin){
        if(loginAtempts > 3){
            return;
        }
        if(loginPin!= userPin){
            loginAtempts++;
            System.out.println("You got the wrong pin, you have " + (3 - loginAtempts) + " attempts left, before your account get's locked!" );
            return;
        }
        login = true;
        if(balance < money){
            System.out.println("Sorry we do not have enough funds at the moment. The maximum you can withdraw is " + balance + ".");
        }
        balance -= money;
        System.out.println("Here you go the G's man " + money + ". Spend it wisely");
        System.out.println("Teste");

    }


    //Atm Getters and Setters

    public int getBalance(){
        return balance;
    }

}
