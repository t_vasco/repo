package org.academiadecodigo.codeforall.MoneyInTheBank;

public class Main {

    public static void main(String[] args){

        Wallet person1Wallet = new Wallet(50);
        PiggyBank person1PiggyBank=new PiggyBank(250);
        Atm person1Atm = new Atm(5000, 11234);
        Person person1 = new Person("Julieta", person1Wallet, person1PiggyBank, person1Atm);


        person1.withdraw(550);
        person1.spend(100);
        person1.spend(300);
        person1.atmWithdraw(50);
    }





}
