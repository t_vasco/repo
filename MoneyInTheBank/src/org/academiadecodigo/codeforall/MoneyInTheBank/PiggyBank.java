package org.academiadecodigo.codeforall.MoneyInTheBank;

public class PiggyBank {

    //PiggyBank properties

    private int moneyInPiggy;

    //Constructor
    public PiggyBank(int moneyInPiggy){
        this.moneyInPiggy = moneyInPiggy;
    }

    //Piggy Bank Methods

    // Deposit money in PiggyBank

    public void deposit(int money){
        System.out.println("I want to add " + money + " to my PiggyBank\n");

        moneyInPiggy += money;

    }

    public int withdraw(int money){
        System.out.println("I want to withdraw " + money + " to my Wallet\n");
        if(money > moneyInPiggy){
            System.out.println("Sorry no can do! You only have " + moneyInPiggy + " available.\n");
            int moneyLeft = moneyInPiggy;
            moneyInPiggy -= moneyInPiggy;

            return moneyLeft;
        }
            moneyInPiggy -= money;
            return money;
    }
    public int getMoneyInPiggy(){
        return moneyInPiggy;
    }

}
