package org.academiadecodigo.codeforall.MoneyInTheBank;

public class Person {

    //Properties of class person
    private String name;
    private Wallet wallet;
    private PiggyBank piggyBank;
    private Atm atm;
    private int upin = 1234;


    //Player name constructor
    public Person(String name, Wallet wallet, PiggyBank piggyBank, Atm pin){
        this.name = name;
        this.wallet = wallet;
        this.piggyBank = piggyBank;
        this.upin = upin;
        this.atm = pin;
    }
    //Methods of class person

    //withdraw money from piggybank to wallet
    private void update(){
        System.out.println("My wallet has " + wallet.getMoneyInWallet());
        System.out.println("My PiggyBank has " + piggyBank.getMoneyInPiggy() + "\n");
    }

    public void withdraw(int money){


        int moneywithdrawn = piggyBank.withdraw(money);
        wallet.fill(moneywithdrawn);
        update();

    }

    //deposit money in piggybank
    public void save(int money){
        wallet.take(money);
        piggyBank.deposit(money);
        update();

    }

    //Spending and rolling the G's
    public void spend(int money){
        wallet.take(money);
        update();
    }

    public void atmWithdraw(int money){
        atm.withdrawMoney(money, upin);
        wallet.fill(money);
        update();
    }

    //Player name Getter
    public String getName(){

        return name;
    }

    public int getUpin(){
        return upin;
    }

}
