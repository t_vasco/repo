package org.academiadecodigo.codeforall.MoneyInTheBank;

public class Wallet {

    //Wallet Properties

    private int moneyInWallet;

    //Constructor
    public Wallet(int moneyInWallet){
        this.moneyInWallet = moneyInWallet;
    }

    //Wallet Methods
    //Spending the money

    public void take(int money){
        System.out.println("I want to spend " + money + " from my Wallet\n");

        if(money > moneyInWallet){
            System.out.println("Sorry no can do!");
            return;
        }
        moneyInWallet -= money;

    }

    //Deposit money

    public int fill(int money){
        moneyInWallet += money;
        return money;
    }

    public int getMoneyInWallet(){
        return moneyInWallet;
    }

}
