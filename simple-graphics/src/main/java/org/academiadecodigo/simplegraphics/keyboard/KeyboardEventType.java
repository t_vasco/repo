package org.academiadecodigo.simplegraphics.keyboard;

/**
 * The type of events supported by the Keyboard
 * @see Keyboard
 */
@SuppressWarnings("unused")
public enum KeyboardEventType {
    KEY_PRESSED,
    KEY_RELEASED
}
