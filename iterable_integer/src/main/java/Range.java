import java.util.Iterator;
import java.util.NoSuchElementException;

public class Range implements Iterable<Integer> {

    private int min;

    private int max;

    public Range(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public Iterator<Integer> iterator() {
        return new RangeIterator();
    }


    private class RangeIterator implements Iterator<Integer> {

        private int current = min -1;

        public boolean hasNext() {
            if (current < max) {
                return true;
            }
            return false;
        }

        public Integer next() {

            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            return ++current;
        }

        public void remove() {
            /*boolean called;
            int currentNum = current;
            for(int j = )*/


        }
    }
}
