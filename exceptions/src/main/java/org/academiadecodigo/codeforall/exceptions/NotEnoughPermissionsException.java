package org.academiadecodigo.codeforall.exceptions;

public class NotEnoughPermissionsException extends FileException {


    public NotEnoughPermissionsException(){
        super("You do not have credentials to do that");

    }

}
