package org.academiadecodigo.codeforall.exceptions;

public class NotEnoughSpaceException extends FileException{


    public NotEnoughSpaceException(){
        super("No Space Availabe");

    }

}
