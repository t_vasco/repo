package org.academiadecodigo.codeforall.exceptions;

public class FileNotFoundException extends FileException {

    public FileNotFoundException(){
       super("I could not find that file man.");

    }

}
