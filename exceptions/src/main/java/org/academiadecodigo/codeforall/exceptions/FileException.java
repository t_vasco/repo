package org.academiadecodigo.codeforall.exceptions;

public class FileException extends Exception {

    public FileException(String text){
        super(text);
    }

}
