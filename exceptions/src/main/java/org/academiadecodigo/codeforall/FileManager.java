package org.academiadecodigo.codeforall;

import org.academiadecodigo.codeforall.exceptions.FileNotFoundException;
import org.academiadecodigo.codeforall.exceptions.NotEnoughPermissionsException;
import org.academiadecodigo.codeforall.exceptions.NotEnoughSpaceException;

public class FileManager {

    //org.academiadecodigo.codeforall.File Manager Methods

    private boolean loggedIn;
    private File[] files;
    private int filesMade;

    public FileManager(int numFiles) {
        this.loggedIn = false;
        this.files = new File[numFiles];
        this.filesMade = 0;
    }


    public void login(){
        loggedIn = true;
        System.out.println("You are logged in.");

    }

    public void logout(){
        loggedIn = false;
        System.out.println("You logout.");

    }

    public File getFile(String name) throws NotEnoughPermissionsException, FileNotFoundException {
        if(!loggedIn){
            throw new NotEnoughPermissionsException();
        }
        for (File file : files) {

            if(file == null){
                break;
            }
            if(name.equals(file.getName())){
                System.out.println("Here you have your file.");
                return file;
            }
        }
        throw new FileNotFoundException();
    }

    public void createFile(String fileName) throws NotEnoughPermissionsException, NotEnoughSpaceException {
        if(!loggedIn){
            throw new NotEnoughPermissionsException();
        }
        if(filesMade == files.length){
            throw new NotEnoughSpaceException();
        }
        for(int i = 0; i < files.length; i++){
            if(files[i] == null){
                files[i] = new File(fileName);
                System.out.println("Your file was created.");
            }
        }
    }
}
