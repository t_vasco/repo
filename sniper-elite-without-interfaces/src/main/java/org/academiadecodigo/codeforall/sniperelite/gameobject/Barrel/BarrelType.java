package org.academiadecodigo.codeforall.sniperelite.gameobject.Barrel;

public enum BarrelType {
    PLASTIC(2),
    WOOD(4),
    METAL(8);

    BarrelType(int maxDamage){
        this.maxDamage = maxDamage;
    }
    private int maxDamage;


    public int getMaxDamage(){

        return maxDamage;
    }

}
