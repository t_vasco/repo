package org.academiadecodigo.codeforall.sniperelite;

import org.academiadecodigo.codeforall.sniperelite.gameobject.Barrel.Barrel;
import org.academiadecodigo.codeforall.sniperelite.gameobject.GameObjectFactory;
import org.academiadecodigo.codeforall.sniperelite.gameobject.GameObject;
import org.academiadecodigo.codeforall.sniperelite.gameobject.decor.Tree;
import org.academiadecodigo.codeforall.sniperelite.gameobject.enemy.Enemy;
import org.academiadecodigo.codeforall.sniperelite.gameobject.weapons.SniperRifle;

/**
 * Sniper Elite
 */
public class Game {

    public static final double ENEMY_PROBABILITY = 0.6;
    public static final int BULLET_DAMAGE = 1;

    private GameObject[] gameObjects;
    private SniperRifle sniperRifle;

    /**
     * Construct the game
     *
     * @param numberObjects the number of game objects to create
     */
    public Game(int numberObjects) {

        this.gameObjects = createGameObjects(numberObjects);
        this.sniperRifle = new SniperRifle(BULLET_DAMAGE);

    }

    /**
     * Start shooting
     */
    public void start() {

        for (GameObject gameObject : gameObjects) {

            System.out.println(gameObject.getMessage());

            if (gameObject instanceof Enemy) {

                Enemy target = (Enemy) gameObject;
                while (!target.isDestroyed()) {

                    System.out.println("Making the shot...");
                    sniperRifle.shoot(target);

                }

                System.out.println("Target is neutralized. I repeat, Target is neutralized!");

            }
            if (gameObject instanceof Barrel) {

                Barrel targetB = (Barrel) gameObject;
                while (!targetB.isDestroyed()) {

                    System.out.println("Making a barrel shot...");
                    sniperRifle.shoot(targetB);

                }

                System.out.println("I found a barrel and felt like practise some shooting!");

            }


        }

        System.out.println("All targets are down. I repeat all targets are down. " + sniperRifle.getShotsFired() + " shots.");

    }

    private GameObject[] createGameObjects(int numberObjects) {

        GameObject[] gameObjects = new GameObject[numberObjects];

        for (int i = 0; i < gameObjects.length; i++) {

            if (Math.random() < ENEMY_PROBABILITY) {

                gameObjects[i] = GameObjectFactory.createEnemy();

            } else {

               gameObjects[i] = GameObjectFactory.createGameObject();
            }

        }

        return gameObjects;
    }

}
