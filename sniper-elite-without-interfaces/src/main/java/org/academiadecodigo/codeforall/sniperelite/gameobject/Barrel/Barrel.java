package org.academiadecodigo.codeforall.sniperelite.gameobject.Barrel;

import org.academiadecodigo.codeforall.sniperelite.Destroyable;
import org.academiadecodigo.codeforall.sniperelite.gameobject.GameObject;

public class Barrel extends GameObject implements Destroyable {

    //Barrel properties
    private BarrelType barrelType;
    private int currentDamage;
    private boolean destroyed;


    public Barrel() {
        this.barrelType = makeBarrel();
    }

    private BarrelType makeBarrel() {
        return BarrelType.values()[(int) (Math.random() * BarrelType.values().length)];
    }

    //Barrel Methods

    //The Hit method to get the barrel
    public void hit(int damage) {

        if (currentDamage > barrelType.getMaxDamage()) {
            destroyed = true;
        }
        currentDamage += damage;

    }

    //Method to check if it is destroyed
    public boolean isDestroyed() {
        return destroyed;
    }


    //Get Message check GameObject
    public String getMessage() {
        return "I am a round barrel.";
    }


}
