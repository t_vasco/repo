package org.academiadecodigo.codeforall.sniperelite.gameobject.enemy;

/**
 * A Soldier enemy without any special behaviour
 */
public class SoldierEnemy extends Enemy {

    /**
     * @see Enemy#Enemy(int)
     */
    public SoldierEnemy(int health) {
        super(health);
    }

}
