package org.academiadecodigo.codeforall.sniperelite;

public interface Destroyable {

    public abstract void hit(int damage);
    public abstract boolean isDestroyed();

}
