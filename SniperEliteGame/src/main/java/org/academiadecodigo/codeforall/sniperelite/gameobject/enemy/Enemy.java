package org.academiadecodigo.codeforall.sniperelite.gameobject.enemy;

import org.academiadecodigo.codeforall.sniperelite.gameobject.Destroyable;
import org.academiadecodigo.codeforall.sniperelite.gameobject.GameObject;

public abstract class Enemy extends GameObject implements Destroyable {

}

/*
* ==Prop==
*
* health and destroyed
*
*
* ==Constructor==
*
* health
*
* ==getter==
*
* Health
*destroyed
* ==Method==
*
* Suffer a hit
*
* override gamemessage
*
* */