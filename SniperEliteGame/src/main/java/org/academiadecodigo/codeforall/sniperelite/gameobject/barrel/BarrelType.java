package org.academiadecodigo.codeforall.sniperelite.gameobject.barrel;

public enum BarrelType {

    PLASTIC,
    WOOD,
    METAL;

    private int maxDamage;


    public int getMaxDamage() {
        return maxDamage;
    }
}

/*
*create enum options
*
* constructor maxDamage
*
*
* getter max Damage
*
* */