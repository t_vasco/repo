

public class Main {
    public static final String STRING = "teste word words test 1 10 1";

    public static void main(String[] args){

        WordHistogram wordHistogram = new WordHistogram(STRING);
        System.out.println("MAP has " + wordHistogram.size() + " distinct words");

        for(String word : wordHistogram){
            System.out.println(word + " : " + wordHistogram.get(word));
        }
    }

}
