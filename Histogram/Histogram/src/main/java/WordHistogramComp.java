import java.util.*;

public class WordHistogramComp implements Iterable<String> {


    private Map<String, Integer> map;


    //Breaks the String in different words - TRY TO USE TERNARY


    public void boxWords(String string) {
        for (String s : string.split(" ")) {
            if (!map.containsKey(s)) {
                map.put(s, 1);
                continue;
            }
            map.put(s, map.get(s) + 1);
        }

    }
    public WordHistogramComp(String string) {
        map = new HashMap<String, Integer>();
        boxWords(string);
    }


    public Iterator<String> iterator() {
        return map.keySet().iterator();
    }

    public int size(){
        return map.size();
    }

    public int get(String word){
        return map.get(word);
    }
}
