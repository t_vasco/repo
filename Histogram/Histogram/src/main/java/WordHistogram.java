import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

public class WordHistogram extends HashMap<String, Integer> implements Iterable<String > {

    public WordHistogram(String s){
        this.boxWords(s);
    }


    public void boxWords(String string) {


        for (String s : string.split(" ")) {

            if (!super.containsKey(s)) {
                super.put(s, 1);
                continue;
            }
            put(s, super.get(s) + 1);
        }
    }

    public Iterator<String> iterator(){

        return keySet().iterator();
    }

}



