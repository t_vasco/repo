package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Position;

public class Mustang extends Car {

    public final static int MOVESBEFOREBREAK = 40;
    public final static int MOVESAFTERBREAK = 10;

    private int moves = 0;

    public Mustang() {super(new Position(), CarType.MUSTANG, 3); }
        @Override
        public void move() {
            moves++;
            if(moves < MOVESBEFOREBREAK || MOVESAFTERBREAK != 0){
                accelerate(chooseDirection());
            }
        }
    }

