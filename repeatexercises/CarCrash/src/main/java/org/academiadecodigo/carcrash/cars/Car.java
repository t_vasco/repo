package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.ColisionDetector;
import org.academiadecodigo.carcrash.field.Direction;
import org.academiadecodigo.carcrash.field.Position;

abstract  public class Car {

    /** The position of the car on the grid */
    private Position pos;

    private int speed;

    private CarType carType;

    private ColisionDetector colisionDetector;

    private int directionChangeLevel = 8;

    private Direction currentDirection;

    private boolean crashed = false;

    abstract public void move();



    public Car(Position pos, CarType carType, int speed){
        this.pos = pos;
        this.carType = carType;
        this.speed = speed;
        currentDirection = Direction.values()[(int) (Math.random()*Direction.values().length)];
    }


    public Position getPos() {
        return pos;
    }


    public boolean isCrashed() {
        return false;
    }



    public void setDirectionChangeLevel(int directionChangeLevel) {
        this.directionChangeLevel = directionChangeLevel;
    }


    public void crashed(){
        this.crashed = true;
    }


    public Direction chooseDirection(){
       Direction newDirection = currentDirection;

       if(Math.random() > ((double)directionChangeLevel)/10){

           newDirection = Direction.values()[(int) (Math.random()*Direction.values().length)];

           if(newDirection.isOposite(currentDirection)){
               return chooseDirection();
           }
       }

       return newDirection;
    }


    public void accelerate(Direction direction){
        if(isCrashed()){
            return;
        }
        Direction newDirection = direction;


        if(pos.isEdge(direction) && newDirection.equals(currentDirection)){
            newDirection = currentDirection.opositeDirection();
        }
        this.currentDirection = newDirection;
        for(int speed = 0; speed < this.speed; speed++){
            getPos().moveInDirection(newDirection, 1);
            if(colisionDetector.isUnsafe(getPos())){
                crashed();
                break;
            }
        }

    }

    public void setColisionDetector(ColisionDetector colisionDetector) {
        this.colisionDetector = colisionDetector;
    }

    @Override
    public String  toString(){
        return isCrashed() ? "C": Character.toString(carType.getSymbol());

    }
}
