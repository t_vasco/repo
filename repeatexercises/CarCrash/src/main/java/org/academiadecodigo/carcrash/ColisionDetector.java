package org.academiadecodigo.carcrash;

import org.academiadecodigo.carcrash.cars.Car;
import org.academiadecodigo.carcrash.field.Position;

public class ColisionDetector {

    private Car[] cars;

    public ColisionDetector(Car[] cars){ this.cars = cars;}

    public boolean isUnsafe(Position pos){
        for(Car c :cars){

            if(c.getPos()!=pos && c.getPos().equals(pos)){
                return true;
            }
        }
        return false;
    }

    public void check(Car car){
        for(Car ic: cars){
            if(ic == car){
                continue;
            }
            if(ic.getPos().equals(car.getPos())){
                ic.crashed();
                car.crashed();
            }
        }
    }
}
