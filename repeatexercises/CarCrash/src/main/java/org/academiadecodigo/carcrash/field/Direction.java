package org.academiadecodigo.carcrash.field;

import static org.graalvm.compiler.loop.InductionVariable.Direction.Up;

public enum Direction {
    UP,
    LEFT,
    RIGHT,
    DOWN;

    public boolean isOposite(Direction dir){
        return dir.equals(opositeDirection());
    }


    public Direction opositeDirection(){

        Direction oposite = Direction.UP;

        switch (this){
            case UP:
                oposite = DOWN;
                break;
            case LEFT:
                oposite =  RIGHT;
                break;
            case RIGHT:
                oposite =  LEFT;
                break;
            case DOWN:
                oposite =  UP;
                break;
        }
       return oposite;
    }

}
