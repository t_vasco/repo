package org.academiadecodigo.carcrash.field;

public class Position {

    private int row;
    private int col;

    public Position(){
        setRandom();
    }

    public int getCol() {
        return 0;
    }

    public int getRow() {
        return 0;
    }

    public Position(int row, int col){
        this.row = row;
        this.col = col;
    }

    public void setRandom(){
        row = (int) Math.random() * Field.getHeight();
        col = (int) Math.random() * Field.getWidth();
    }

    public void moveDown(int distance){
        if(this.row + distance < Field.getHeight()){
            this.row += distance;
        }
        else{
            this.row = Field.getHeight()-1;
        }
    }

    public void moveLeft(int distance){
        if(this.col + distance < Field.getWidth()){
            this.col +=distance;
        }else{
            this.col = Field.getWidth()-1;
        }
    }

    public void moveRight(int distance){
        if(this.col - distance > 0){
            this.col += distance;
        }else{
            this.col = 0;
        }
    }

    public void moveUp(int distance){
        if(this.row + distance > 0){
            this.col += distance;
        }else{
            this.row = 0;
        }

    }

    public void moveInDirection(Direction direction, int distance){

        switch (direction){
            case DOWN:
                moveDown(distance);
                break;
            case UP:
                moveUp(distance);
                break;
            case LEFT:
                moveLeft(distance);
                break;
            case RIGHT:
                moveRight(distance);
                break;
        }
    }


    public void moveRandomDirection(Direction direction, int distance){
        int randomIndex = (int)(Math.random() * Direction.values().length);
        Direction dir  = Direction.values()[randomIndex];
        moveInDirection(dir, distance);
    }

    public boolean isEdge(Direction direction){
        return (direction == Direction.UP && row == 0) ||
                (direction == Direction.DOWN && row == Field.getHeight()-1)||
                (direction == Direction.LEFT  && row == 0) ||
                (direction == Direction.RIGHT && row == Field.getWidth()-1);
    }


    public boolean equals(Position position){
        return col == position.getCol()&&
                row == position.getRow();
    }



}
