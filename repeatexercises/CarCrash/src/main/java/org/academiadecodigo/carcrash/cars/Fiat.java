package org.academiadecodigo.carcrash.cars;

import javafx.geometry.Pos;
import org.academiadecodigo.carcrash.field.Position;

public class Fiat extends Car{

    public final static int MOVESBEFOREBREAK= 30;
    public final static int MOVESAFTERBREAK = 4;

    private int moves = 0;

    public Fiat(){super(new Position(), CarType.FIAT, 1);}

        @Override
        public void move() {
            moves++;
        if(moves < MOVESBEFOREBREAK || moves % MOVESAFTERBREAK != 0){
            accelerate(chooseDirection());
        }
    }
}





