package Grid;

public abstract class AbstractGridPos  {

    private int col;
    private int row;

    private Grid grid;

    public AbstractGridPos(int col, int row, Grid gird){
        this.col = col;
        this.row = row;
        this.grid = grid;
    }

    public Grid getGrid(){
     return grid;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public void moveInDirection(GridDirection direction, int distance){

        col += direction.getDeltaCol();
        row += direction.getDeltaRow();




    }

    public void moveUP(int distance){

    }

    public void moveDOWN(int distance){

    }

    public void moveLEFT(int distance){

    }

    public void moveRight(int distance){

    }
}