package Grid;


import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class SimpleGfxGrid implements Grid {

    private int cols;
    private int rows;

    private Rectangle field;


    public int getCols() {
        return cols;
    }

    public int getRows() {
        return rows;
    }

   public SimpleGfxGrid(int cols, int rows){
        this.cols = cols;
        this.rows = rows;
   }

    @Override
    public Grid makeGrid() {
        return null;
    }

    @Override
    public Grid makeGrid(int cols, int rows) {
        return null;
    }


    public void field (int cols, int rows){
        this.field = new Rectangle(cols * CELL_SIZE + PADDING, rows * CELL_SIZE + PADDING, CELL_SIZE, CELL_SIZE );
        field.draw();
    }






}
