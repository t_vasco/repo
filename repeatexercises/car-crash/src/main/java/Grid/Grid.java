package Grid;


interface Grid {

    public static final int CELL_SIZE = 15;
    public static final int PADDING = 10;

    public Grid makeGrid();

    public Grid makeGrid(int cols, int rows);

}
