import Grid.SimpleGfxGrid;

public class Game {

    private int cols;
    private int rows;

    private int delay;

    private SimpleGfxGrid grid;

    public Game(int cols, int rows, int delay){
        this.cols = cols;
        this.rows = rows;
        this.delay = delay;
        this.grid = new SimpleGfxGrid(cols, rows);

    }


    public void init(){

    }


    public void start(){
        grid.field(cols, rows);
    }


}
