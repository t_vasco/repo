
var characterData = [
    { "id": 1, "name": "Luke Skywalker", "birth_year": "19BBY", "gender": "male", "height": "172", "mass": "77" },
    { "id": 2, "name": "C-3PO", "birth_year": "112BBY", "gender": "n/a", "height": "167", "mass": "75" },
    { "id": 3, "name": "R2-D2", "birth_year": "33BBY", "gender": "n/a", "height": "96", "mass": "32" },
    { "id": 4, "name": "Darth Vader", "birth_year": "41.9BBY", "gender": "male", "height": "202", "mass": "136" },
    { "id": 5, "name": "Leia Organa", "birth_year": "19BBY", "gender": "female", "height": "150", "mass": "49" },
    { "id": 6, "name": "Owen Lars", "birth_year": "52BBY", "gender": "male", "height": "178", "mass": "120" },
    { "id": 7, "name": "Beru Whitesun Lars", "birth_year": "47BBY", "gender": "female", "height": "165", "mass": "75" },
    { "id": 8, "name": "R5-D4", "birth_year": "unknown", "gender": "n/a", "height": "97", "mass": "32" },
    { "id": 9, "name": "Biggs Darklighter", "birth_year": "24BBY", "gender": "male", "height": "183", "mass": "84" },
    { "id": 10, "name": "Obi-Wan Kenobi", "birth_year": "57BBY", "gender": "male", "height": "182", "mass": "77" }
];


function buildTable(data){
    var table = document.createElement("table");

    var thead = document.createElement("thead");
    var tbody = document.createElement("tbody");
    var headRow = document.createElement("tr");
    ["Id", "Name", "Birth Year", "Gender", "Height", "Mass"].forEach(function(el){
        var th = document.createElement("th");
        th.appendChild(document.createTextNode(el));
        headRow.appendChild(th);
    });

    thead.appendChild(headRow);
    table.appendChild(thead);

    data.forEach(function(el){
        var tr = document.createElement("tr");
        for(var o in el){
            var td = document.createElement("td");
            td.appendChild(document.createTextNode(el[o]));
            tr.appendChild(td);
        }
        tbody.appendChild(tr);
    });
    table.appendChild(tbody);
    return table;
}
function load(){
        document.getElementById("user-table").appendChild(buildTable(characterData));
    };