import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyFile {

    public static void main(String[] args) {

        FileInputStream inputStream = null;
        FileOutputStream outputStream = null;




        try{
            File file = new File("/home/tiagovasco/personal_repo/IOExercises/src/main/resources/text3.txt");

            File outFile = new File("/home/tiagovasco/personal_repo/IOExercises/src/main/resources/text2.txt");


            inputStream = new FileInputStream(file);
            outputStream = new FileOutputStream(outFile);

            byte[] buffer = new byte[1024];

            int length;

            while((length = inputStream.read(buffer)) > 0){
                outputStream.write(buffer, 0, length);
            }

            inputStream.close();
            outputStream.close();

            System.out.println("File Copied successfully!!");


        }catch (IOException ex){
            System.out.println("Exception found");
            ex.printStackTrace();
        }



    }
}
