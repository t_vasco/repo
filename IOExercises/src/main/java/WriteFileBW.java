import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class WriteFileBW {

    public static void main(String[] args) {

        BufferedWriter bw = null;
        File file = null;

        String myContent = "This is another content that I am writing to the file using a Buffered Writer";

        try {
            file = new File("/home/tiagovasco/personal_repo/IOExercises/src/main/resources/text3.txt");

            if(!file.exists()){
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file);
            bw = new BufferedWriter(fw);

            bw.write(myContent);
            System.out.println("File Written Successfully");

        }catch (IOException ex){
            System.out.println("Exception writing file:");
            ex.printStackTrace();
        }
        finally {
            try{
                if(bw != null){
                    bw.close();
                }
            } catch (IOException ex){
                System.out.println("Exception closing: ");
                ex.printStackTrace();
            }
        }
    }

}
