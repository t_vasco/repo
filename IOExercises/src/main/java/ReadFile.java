
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ReadFile {



    public static void main(String[] args) {

        BufferedInputStream bis = null;
        FileInputStream fis = null;

        try{
            fis = new FileInputStream("/home/tiagovasco/personal_repo/IOExercises/src/main/resources/text.txt");

            bis = new BufferedInputStream(fis);


            while(bis.available() > 0){
                System.out.print((char) (bis.read()));
            }


        }catch (FileNotFoundException e){
            System.out.println("File not found " + e);
        }catch (IOException e){
            System.out.println("IO Exception " + e);
        }
        finally {
            try{
                if(bis != null && fis != null){
                    fis.close();
                    bis.close();
                }
            }catch (IOException e){
                System.out.println("IO Exception Found in Closing " + e);
            }
        }
    }
}
