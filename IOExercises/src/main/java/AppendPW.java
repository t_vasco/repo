import java.io.*;

public class AppendPW {

    public static void main(String[] args) {

        try{
            File file = new File("/home/tiagovasco/personal_repo/IOExercises/src/main/resources/text3.txt");

            if(!file.exists()){
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file, true);

            BufferedWriter bw = new BufferedWriter(fw);

            PrintWriter pw = new PrintWriter(bw);

            pw.println(" ");
            pw.println(" This is the first Line");
            pw.println("This is the second Line");
            pw.println("This is the final line");
            pw.println("! =)");

            pw.close();

            System.out.println("Data Successfully appended at the end of the file");
        }catch (IOException ex){
            System.out.println("Exception writing file: ");
            ex.printStackTrace();
        }


    }
}
