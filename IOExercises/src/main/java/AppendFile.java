import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class AppendFile {

    public static void main(String[] args) {
        try{
            String content = "This is my content which would be appended " +
                    "at the end of the specified file";
            File file = new File("/home/tiagovasco/personal_repo/IOExercises/src/main/resources/text3.txt");

            if(!file.exists()){
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(content);

            bw.close();

            System.out.println("Data successfully appended at the end of the file");
        }catch (IOException ex){
            System.out.println("Error appending to file: ");
            ex.printStackTrace();
        }

    }
}
