import java.io.File;

public class DeleteFile {

    public static void main(String[] args) {
        try {
            File file = new File("/home/tiagovasco/personal_repo/IOExercises/src/main/resources/text.txt");

            if(file.exists()){
                file.delete();
                System.out.println(file.getName() + " is deleted!");
            }else {
                System.out.println("Delete File : failed;");
            }

        }catch (Exception ex){
            System.out.println("Exception found: ");
            ex.printStackTrace();
        }
    }

}
