import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class BufferedReaderex {


    public static void main(String[] args) {
        BufferedReader br = null;
        BufferedReader br2 = null;
        FileReader fr = null;


        try{
            fr = new FileReader("/home/tiagovasco/personal_repo/IOExercises/src/main/resources/text.txt");
            br = new BufferedReader(fr);

            System.out.println("Reading the file using ReadLine() method:");
            String contentLine = br.readLine();
            while (contentLine != null){
                System.out.println(contentLine);
                contentLine = br.readLine();
            }

            fr = new FileReader("/home/tiagovasco/personal_repo/IOExercises/src/main/resources/text2.txt");
            br2 = new BufferedReader(fr);

            System.out.println("Read File using Read() Method:");
            int num = 0;
            char ch;

            while ((num = br2.read()) != -1){
                ch = (char)num;
                System.out.println(ch);
            }

        }catch (IOException e){
            e.printStackTrace();
        }
        finally {
            try{
                if(br != null){
                    br.close();
                }
                if(br2 != null){
                    br2.close();
                }
            } catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}
