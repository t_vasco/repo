import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class WriteFileFOS {

    public static void main(String[] args) {
        FileOutputStream fos = null;

        File file;

        String mycontent = "This is my Data which needs" +
                " to be written into the file";

        try{
            file = new File("/home/tiagovasco/personal_repo/IOExercises/src/main/resources/text3.txt");
            fos = new FileOutputStream(file);

            if(!file.exists()){
                file.createNewFile();
            }

            byte[] bytesArray = mycontent.getBytes();
            fos.write(bytesArray);
            fos.flush();
            System.out.println("File Written Successfully");
        }catch (IOException ex){
            System.out.println("Exception found: ");
            ex.printStackTrace();
        }
        finally {
            try{
                if(fos != null){
                    fos.close();
                }
            }catch (IOException ex){
                System.out.println("Exeption found closing: ");
                ex.printStackTrace();
            }
        }
    }


}
