
What is Lorem Ipsum?

Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
Why do we use it?

It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).

Where does it come from?

Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.

The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
Where can I get some?

There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.

	paragraphs
	words
	bytes
	lists
		Start with 'Lorem
ipsum dolor sit amet...'


Translations: Can you help translate this site into a foreign language ? Please email us with details if you can help.
There are now a set of mock banners available here in three colours and in a range of standard banner sizes:
BannersBannersBanners
Donate: If you use this site regularly and would like to help keep the site on the Internet, please consider donating a small sum to help pay for the hosting and bandwidth bill. There is no minimum donation, any sum is appreciated - click here to donate using PayPal. Thank you for your support.
Firefox Add-on NodeJS TeX Package Python Interface GTK Lipsum Rails .NET Groovy Adobe Plugin
The standard Lorem Ipsum passage, used since the 1500s

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC

"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"
1914 translation by H. Rackham

"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"
Section 1.10.33 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC

"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."
1914 translation by H. Rackham

"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."


n descriptive writing, the author does not just tell the reader what was seen, felt, tested, smelled, or heard. Rather, the author describes something from their own experience and, through careful choice of words and phrasing, makes it seem real. Descriptive writing is vivid, colorful, and detailed.
Good Descriptive Writing

Good descriptive writing creates an impression in the reader's mind of an event, a place, a person, or a thing. The writing will be such that it will set a mood or describe something in such detail that if the reader saw it, they would recognize it.

To be good, descriptive writing has to be concrete, evocative and plausible.

    To be concrete, descriptive writing has to offer specifics the reader can envision. Rather than "Her eyes were the color of blue rocks" (Light blue? Dark blue? Marble? Slate?), try instead, "Her eyes sparkled like sapphires in the dark."

    To be evocative, descriptive writing has to unite the concrete image with phrasing that evokes the impression the writer wants the reader to have. Consider "her eyes shone like sapphires, warming my night" versus "the woman's eyes had a light like sapphires, bright and hard." Each phrase uses the same concrete image, then employs evocative language to create different impressions.

    To be plausible, the descriptive writer has to constrain the concrete, evocative image to suit the reader's knowledge and attention span. "Her eyes were brighter than the sapphires in the armrests of the Tipu Sultan's golden throne, yet sharper than the tulwars of his cruelest executioners" will have the reader checking their phone halfway through. "Her eyes were sapphires, bright and hard" creates the same effect in a fraction of the reading time. As always in the craft of writing: when in doubt, write less.

Examples of Descriptive Writing

The following sentences provide examples of the concreteness, evocativeness and plausibility of good descriptive writing.

    Her last smile to me wasn't a sunset. It was an eclipse, the last eclipse, noon dying away to darkness where there would be no dawn.

    My Uber driver looked like a deflating airbag and sounded like talk radio on repeat.

    The old man was bent into a capital C, his head leaning so far forward that his beard nearly touched his knobby knees.

    The painting was a field of flowers, blues and yellows atop deep green stems that seemed to call the viewer in to play.

    My dog's fur felt like silk against my skin and her black coloring shone, absorbing the sunlight and reflecting it back like a pure, dark mirror.

    The sunset filled the sky with a deep red flame, setting the clouds ablaze.

    The waves rolled along the shore in a graceful, gentle rhythm, as if dancing with the land.

    Winter hit like a welterweight that year, a jabbing cold you thought you could stand until the wind rose up and dropped you to the canvas.

Examples of Descriptive Text in Literature

Because descriptive text is so powerful, many examples of it can be found in famous literature and poetry.
The High Window

The mystery novelist Raymond Chandler was one of American literature's masters of descriptive language. This sentence from The High Window strikes the perfect notes to embody its subject:

    "She had pewter-colored hair set in a ruthless permanent, a hard beak, and large moist eyes with the sympathetic expression of wet stones."

Life in the Iron Mills

Notice the vivid description of smoke in this excerpt from Rebecca Harding Davis's Life in the Iron Mills:

    "The idiosyncrasy of this town is smoke. It rolls sullenly in slow folds from the great chimneys of the iron-foundries, and settles down in black, slimy pools on the muddy streets. Smoke on the wharves, smoke on the dingy boats, on the yellow river--clinging in a coating of greasy soot to the house-front, the two faded poplars, the faces of the passers-by."

Jamaica Inn

In this excerpt from Jamaica Inn by Daphne du Maurier, notice the writer's choice of adjectives, adverbs, and verbs. Granite. Mizzling. Du Maurier's choice of words allows the reader to almost feel the weather occurring on the page.

    "It was a cold grey day in late November. The weather had changed overnight, when a backing wind brought a granite sky and a mizzling rain with it, and although it was now only a little after two o'clock in the afternoon the pallor of a winter evening seemed to have closed upon the hills, cloaking them in mist."

The Eagle

In Alfred Tennyson's "The Eagle," he conveys power and majesty in just a few lines:

    "He clasps the crag with crooked hands;
    Close to the sun in lonely lands,
    Ring'd with the azure world, he stands.
    The wrinkled sea beneath him crawls;
    He watches from his mountain walls,
    And like a thunderbolt he falls."

Descriptive Text in Songs

Descriptive text examples can also be found in many songs, since songs are meant to capture your emotions and to invoke a feeling.
Through the Strings of Infinity

Some of the most vivid and effective descriptive writing in music can be found in rap. The evocation of alienation and the need to create in "Through the Strings of Infinity" by Canibus is truly poetic.

    "I was born in an empty sea, My tears created oceans
    Producing tsunami waves with emotions
    Patrolling the open seas of an unknown galaxy
    I was floating in front of who I am physically
    Spiritually paralyzing mind body and soul
    It gives me energy when I'm lyrically exercising
    I gotta spit 'til the story is told in a dream by celestial bodies
    Follow me baby"

Windowpane

The heavy metal band Opeth uses vivid descriptive writing to evoke loneliness in their song "Windowpane."

    "Blank face in the windowpane
    Made clear in seconds of light
    Disappears and returns again
    Counting hours, searching the night"

Blank Space

In her hit song "Blank Space," Taylor Swift uses concrete, evocative descriptions to evoke two very different impressions.

First:

    "Cherry lips, crystal skies
    I can show you incredible things
    Stolen kisses, pretty lies
    You're the king, baby, I'm your queen"

Then:

    "Screaming, crying, perfect storm
    I can make all the tables turn
    Rose gardens filled with thorns
    Keep you second guessing"

Related articles on YourDictionary

    Sonnet Examples
    Examples of Imagery
    Tone Word Examples
    More articles

All in the Details

Now that you have several different examples of descriptive text, you can try your hand at writing a detailed, descriptive sentence, paragraph or short story of your own. If you need help with powerful descriptions, try some figurative language to help to paint a picture and evoke emotions.

If you need inspiration, explore the authors linked above, or check out our quotes from poets like "H.D." Hilda Doolitle and Gerard Manley Hopkins, novelists like Angela Carter, or songwriters like Tori Amos and Tom Waits.
Bright red and orange sunrise
The sunset filled the sky with a deep red flame, setting the clouds ablaze.
YourDictionary definition and usage example. Copyright © 2018 by LoveToKnow Corp

    Link/Cite

Link to this page

Cite this page

MLA Style

"Descriptive Text Examples." YourDictionary, n.d. Web. 24 June 2019. <http://examples.yourdictionary.com/descriptive-text-examples.html>.

APA Style

Descriptive Text Examples. (n.d.). Retrieved June 24th, 2019, from http://examples.yourdictionary.com/descriptive-text-examples.html
Do you have a good example to share? Add your example here.