
var finalUrl = 'https://swapi.co/api/vehicles/?page=1';


function generateCard(name, people, speed) {

    var card = ('<div id="card" class="w3-third" style="margin-top: 20px">' +
        '<div class= "w3-card" style = "background-image: url(\'img/space_background.jpg\')">' +
        '<div class="w3-container" onclick="selected()">' +
        '<h5>Ship Name: ' + name + '</h5>' +
        '<h5>Num Crew: ' + people + '</h5>' +
        '<h5>Max Speed: ' + speed + '</h5>' +
        '<button></button>' +
        '</div>' +
        '</div>' +
        '</div >');
    $("#insertCard").append(card);
}


function spaceshipInfo(response){
    var shipsInObj = response.results.length;
    finalUrl = response.next;   
    var shipsInserted = 0;
    console.log(response);

    while (shipsInserted < shipsInObj) {
        var name = response.results[shipsInserted].name;
        var people = response.results[shipsInserted].crew;
        var speed = response.results[shipsInserted].max_atmosphering_speed;
        generateCard(name, people, speed);
        shipsInserted++;
    }

    if (shipsInserted < response.count) {
        makeRequest();
    }
}


function planetInfo(response){

}

function successCallback(response) {
    if (finalUrl.indexOf("vehicles") >= 0){
    spaceshipInfo(response);
    }
    if(finalUrl.indexOf("planets") >= 0){
        planetInfo(response);
    }
}



function errorCallback(request, status, error) {
    // do something with the error
}

function selected(){
    console.log("here");

    $(document).ready(function(){
        $("#insertCard").remove();
        finalUrl = "https://swapi.co/api/planets/";
    });

}



// perform an ajax http get request
function makeRequest() {
    $.ajax({
        url: finalUrl,
        async: true,
        success: successCallback,
        error: errorCallback
    });
}