package org.academiadecodigo.bootcamp.containers;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<T> implements Iterator<T> {

    private T t;

    public void set(T t) {
        this.t = t;
    }

    public T get() {
        return t;
    }

    private Node head;
    private int length = 0;

    public LinkedList() {
        this.head = new Node(null);
    }

    public int size() {
        return length;
    }

    /**
     * Adds an element to the end of the list
     *
     * @param data the element to add
     */
    public void add(T data) {

        Node node = new Node(data);
        Node iterator = head;
        while (iterator.getNext() != null) {
            iterator = iterator.getNext();
        }
        iterator.setNext(node);
        length++;

    }

    /**
     * Obtains an element by index
     *
     * @param index the index of the element
     * @return the element
     */
    public T get(int index) {

     /*   Node iterator = head.getNext();

        while(iterator != null){
            if(index == 0){
                return iterator.getData();
            }
            iterator = iterator.getNext();
            index--;
        }*/

        Node iterator = head.getNext();
        int iteratorCount = 0;

        while (iterator != null) {
            if(iteratorCount == index) {

                return iterator.getData();
            }
            iterator = iterator.getNext();
            iteratorCount++;
        }

        return null;
    }

    /**
     * Returns the index of the element in the list
     *
     * @param data element to search for
     * @return the index of the element, or -1 if the list does not contain element
     */
    public int indexOf(T data) {

      /*  int index = 0;
        Node iterator = head.getNext();

        while (iterator != null) {
            if (iterator.getData().equals(data)) {
                return index;
            }
            iterator = iterator.getNext();
            index++;
        }
        return -1;*/

         Node node = new Node(data);
        Node aIterator = head;
        int iteratorCount = 0;

        if (!aIterator.getData().equals(null)) {
            while (!aIterator.getData().equals(data)) {
                aIterator = aIterator.getNext();
                iteratorCount++;
            }
            return iteratorCount;
        }
        return -1;

    }

    public Iterator<T> iterator(){
        return new LinkedListIterator();

    }

    private class LinkedListIterator implements Iterator<T>{

        private Node iterator = head;
        private boolean canRemove = true;

        @Override
        public boolean hasNext(){
            return iterator.getNext() != null;

        }

        @Override
        public T next() {

            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            iterator = iterator.getNext();
            canRemove = true;
            return iterator.getData();



        }

        @Override
        public void remove(){
            if(!canRemove){
                throw new IllegalStateException();
            }
            canRemove = false;
            LinkedList.this.remove(iterator.getData());

        }
    }
    /**
     * Removes an element from the list
     *
     * @param data the element to remove
     * @return true if element was removed
     */
    public boolean remove(T data) {

        Node previous = head;
        Node current = head.getNext();

        while (current != null) {
            if (current.getData().equals(data)) {
                previous.setNext(current.getNext());
                length--;
                return true;
            }
            previous = current;
            current = current.getNext();
        }
        return false;

    }

    private class Node {

        private T data;
        private Node next;

        public Node(T data) {
            this.data = data;
            next = null;
        }

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }

}
