package org.academiadecodigo.bootcamp.tutorial;

public class Tutorial {

    //////////////////////////////////////// Constructor methods ////////////////////////////////////////

    // In Java, every class has a special method that is invoked the moment we create an instance of that class.
    // This method is known as the constructor method.
    // The constructor method has a couple of rules attached to both its declaration and usage.

    // The constructor method can be easily identified in the class's declaration by 2 major differences compared to
    // the remaining methods:

    //// It has the same name as the class (in this case it'd be called Tutorial), with the same capitalization as well.
    //// This means that the constructor method should be the only method with it's name beginning with an upper case
    //// letter, according to the convention (which we should follow).

    //// Besides that, it is also the only method which does not have a return type defined in it's declaration:

    // This is a constructor method declaration, notice there is no return type preceding the method's name.
    public Tutorial() {

    }

    // This is NOT a constructor method. Despite it having the same name as the class, it has a return type
    // This is why convention is important, otherwise things can get confusing pretty fast.
    public int Tutorial() {
        return 0;
    }

    // Another rule we must follow when declaring our constructor methods is that they cannot return a value.

    //////////////////////////////////// Using constructors //////////////////////////////////////////


    // In order to make use of a constructor method, we do what we regularly do with any method; we invoke it!
    // There is a small catch though. The invocation of a constructor method is usually preceded by the keyword new.
    // So, how would we create an instance of the class NoConstructorDeclared?

    public void demo1() {
        NoConstructorDeclared instance        =           new NoConstructorDeclared();
        //|---variable declaration---| |--assignment--|  |---constructor invocation---|

    }

    // Lets look a little closer at the constructor invocation:

    ////               new                               NoConstructorDeclared()
    //// |-----creates a new object-----| |------initializes the newly created object-------|

    // This is enough of an explanation on what's happening for now.
    // So, the new operator will create a new object and the invocation of the constructor method will
    // simply initialize this object. But what do we mean by "initializing" an object?
    // Well, most classes have fields(properties), and we usually want to assign a value to those fields when creating an object.

    // Say you have a class Person with the non-static private field String name declared.
    // When we create an instance of the Person class, we'll most likely want each person to have an assigned value
    // to it's field name.
    // When we assign a value for the first time to a variable, we say we're initializing that variable.
    // So if a class of ours has multiple fields, we can initialize them the moment we're creating it.
    // This is what is knows as initializing an object.

    // More importantly, this is what the constructor method should be used for! The constructor method's body is the
    // ideal place to initialize our fields by assigning a value to them.

    // We'll get back to this in a while, but first, we need to talk about the NoConstructorDeclared class.



    //////////////////////// Default constructor //////////////////////////////


    // Take a look at the NoConstructorDeclared class. It's pretty empty, isn't it?
    // It has absolutely nothing declared, no methods, no fields, nothing.
    // So how come we're invoking a method from that class on line 41?
    // The constructor method we're invoking is not declared in this class. How come?

    // Every time we declare a new class, we're always capable of creating new instances of it.
    // This is because Java provides us with a default constructor for every class declared in our application.
    // The default constructor for the NoConstructorDeclared class (the one we're invoking in line 41) looks like this:

    // public NoConstructorDeclared() {
    //
    // }

    // It looks like it does nothing, right? And you're not completely wrong, but let's think about why that's so.
    // If I told you I had a class in my application that was called Dog, could you know for certain what fields it would have?
    // If you're saying yes, you're either lying, wrong or both! Sorry!
    // You have no way of knowing which fields I'm going to have in this new class. I could have a field that would store
    // this particular dog's name, the dog's race, height, weight, eye color, favourite food, or none of it!
    // More than that, you wouldn't know the type of these fields. Is the dog's weight field a double, a float, an int,
    // a wrapper class of any of these primitives?
    // What about the race field? Does it hold a String, is it a reference to a class Race I have somewhere in my application?
    // No idea. What if in my application, every dog would have the same name? Maybe every instance
    // of dog has the value of their field String name holding a reference to the String "Fido".

    // As you can see, it would be unwise for the default constructor to make any assumptions on what its behaviour
    // would be. So bottom line, the default constructor initializes nothing because it can make no assumptions on
    // what the class it belongs to will need initialized, nor how it will be initialized.




    ////////////////////////////// Implementing your own constructor ////////////////////////////////////


    // Now, let's take a look at the class ConstructorDeclared.
    // This class has a constructor declared that does exactly what we've been talking about.
    // It initializes a field, AKA, it assigns a value for the first time to a variable.
    // In this case this variable is the private non-static field String name.

    // This constructor is pretty swell, but there's a catch though. After we declare our own constructor
    // we can no longer invoke the default constructor. It is called the default constructor because it is
    // the one that is available to us when we have not yet declared our own.

    // The good thing is that we can have multiple constructor methods for the same class.
    // All we need to do is declare both!

    //public class ConstructorDeclared {

    //    private String name;
    //
    //    public ConstructorDeclared(String name) {
    //        this.name = name;
    //    }
    //
    //    public ConstructorDeclared() {
    //
    //    }
    //}

    // This is what the ConstructorDeclared class would look like with both the default constructor and
    // our own constructor declared. Pretty neat!

    // So now, how would we invoke the constructor we declared before? Pretty much the same way we invoke any method,
    // but with the added condition that the operator new must proceed the invocation
    // (just like when we invoked the default constructor method):


    public void demo2() {
        ConstructorDeclared instance          =          new ConstructorDeclared("John Doe");
        //|---variable declaration---| |--assignment--| |-------constructor invocation-------|
    }

    // As a closing note, there are a couple of things we should not do inside a constructor method.
    // This is usually any operation that creates objects that have an heavy instantiation logic,
    // creating a considerable number of objects, logic that requires the usage of loops, etc.
    // A constructor method should serve the purpose of initializing properties, if initializing a property
    // requires us to run complex logic, that should be relegated to another separate method that should be
    // invoked AFTER we instantiate the object (like the init method in Car Crash!).

    // That concludes our tutorial for constructor methods.
    // There is more to know, especially when it comes to inheritance, but these are the basics.
    // On to the exercise class then!
}
