
var ajax = new XMLHttpRequest();
var input;

ajax.onreadystatechange = function () {
    if (ajax.readyState === 4 && ajax.status === 200) {
        var response = JSON.parse(ajax.responseText);

        createVar(response);
    }
};


function createVar(response){

    var avatar = response.items[0].avatar_url;
    var userName = response.items[0].login;
    var userId = response.items[0].id;
    var repo = response.items[0].repos_url;
    console.log(response);
    console.log(response.length + " This is the length");
    populate(avatar, userName, userId, repo);
}

function populate(avatar, userName, userId, repo) {
    document.getElementById("userPhoto").src = avatar;
    document.getElementById("userId").innerHTML = "User id: " + userId;
    document.getElementById("userName").innerHTML = "User name: " + userName;
    document.getElementById("formId").action = repo;
}

function getInput() {
    input = document.getElementById("user-input").value;
    document.getElementById("output").innerHTML = "You wrote: " + input;
}

function submited() {
    if (input == "") {
        alert("No input received!");
        return;
    }
    ajax.open('GET', 'https://api.github.com/search/users?q=' + input, true);
    ajax.setRequestHeader('Content-type', 'application/json');
    ajax.send();
}
